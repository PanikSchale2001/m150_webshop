-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE `application_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activated` bit(1) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `cart` (
  `id` varchar(255) NOT NULL,
  `total_amount` double NOT NULL,
  `total_amount_with_tax` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `product_fk` bigint(20) DEFAULT NULL,
  `products_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7x4wqwxqlysreqmoggoy0xpxs` (`product_fk`),
  KEY `FKpls3apnfag25g6oj0054h09fx` (`products_id`),
  CONSTRAINT `FK7x4wqwxqlysreqmoggoy0xpxs` FOREIGN KEY (`product_fk`) REFERENCES `product` (`id`),
  CONSTRAINT `FKpls3apnfag25g6oj0054h09fx` FOREIGN KEY (`products_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double NOT NULL,
  `sales_tax` double NOT NULL,
  `weight` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `product_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `count` int(11) NOT NULL,
  `cart_id` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK18k4n09k19bbcnw7tib8douou` (`cart_id`),
  KEY `FKewq69r22l90ckjdkn8ttvb7j3` (`product_id`),
  CONSTRAINT `FK18k4n09k19bbcnw7tib8douou` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  CONSTRAINT `FKewq69r22l90ckjdkn8ttvb7j3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2020-12-05 22:01:58