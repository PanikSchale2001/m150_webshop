-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

INSERT INTO `image` (`id`, `name`, `path`, `product_fk`, `products_id`) VALUES
(1,	'test1',	'test1.png',	1,	1),
(2,	'test2',	'test2.png',	2,	2),
(3,	'rooibos_caramel',	'rooibos_caramel.jpg',	3,	3),
(4, 'irish_breakfast', 'irish_breakfast.jpg', 4,4),
(5, 'goettermischung', 'goettermischung.jpg', 5,5),
(6, 'mango', 'mango.jpg', 6,6),
(7, 'assam_halmari', 'assam_halmari.jpg', 7,7),
(8, 'earl_grey', 'earl_grey.jpg', 8,8),
(9, 'spiced_black_tea', 'spiced_black_tea.jpg', 9,9),
(10, 'starker_friese', 'starker_friese.jpg', 10,10)
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `name` = VALUES(`name`), `path` = VALUES(`path`), `product_fk` = VALUES(`product_fk`), `products_id` = VALUES(`products_id`);

INSERT INTO `product` (`id`, `description`, `name`, `price`, `sales_tax`, `weight`) VALUES
(1,	'A tea like no other. Try now the new Premium  Platinum Highest Grade Royally English Earl Tea V2',	'Royally English Earl Tea V2',	99.95, 0.025,	0.05),
(2,	'The cheaper option for people with no royal wallet', 'Peasant Fruit Tee',	10.5, 0.025,	1),
(3,	'Rooibos with the crème caramel taste',	'Rooibos Caramel',	20, 0.025, 0.250),
(4,	'A strong breakfast mix.',	'Irish Breakfast',	38.00, 0.025, 0.500),
(5,	'Exotic blend, Darj. Ceylon blend with Chinese. Rose and jasmine tea rounded off with aromas of orange, bergamot, lychee and mango',	'Mixture of gods',	63, 0.025, 1),
(6,	'Fruity tropical feeling in the tea cup.',	'mango tea',	7, 0.025,	0.1),
(7,	'Finest Assam from Dibrugarh District. Its taste is balanced and full with a spicy, malty character. Its cup color is bright deep copper and the aroma is malty and strong.',	'Assam Halmari TGFOP',	72, 0.025,	1),
(8,	'Classic black tea. A tea flavored with bergamot oil.',	'Earl Grey',	7, 0.025,	0.1),
(9,	'A Taste of India, This Chai mixture has more cloves than Mumbaichai.',	'Spiced Black Tea',	17.5, 0.025,	0.25),
(10, 'Strong and strong friese mixture. Assam-Broken. Aromatic, full of the dark cup the Friesians drink.',	'Strong Friese',	63, 0.025,	1)
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `description` = VALUES(`description`), `name` = VALUES(`name`), `price` = VALUES(`price`), `weight` = VALUES(`weight`);

-- 2020-12-03 20:19:14
