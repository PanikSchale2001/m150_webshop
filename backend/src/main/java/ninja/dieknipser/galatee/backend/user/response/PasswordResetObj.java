package ninja.dieknipser.galatee.backend.user.response;

/**
 * A class used to reset the password
 *
 * @author sebi
 *
 */
public class PasswordResetObj {
	private String password;

	/**
	 * constructor
	 */
	public PasswordResetObj() {
	}

	/**
	 * constructor
	 *
	 * @param password the password
	 */
	public PasswordResetObj(String password) {
		this.password = password;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
