package ninja.dieknipser.galatee.backend.configuration.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.security.RolesAllowed;

/**
 * A spring boot meta annoation for logged in users
 *
 * @author sebi
 *
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@RolesAllowed("ROLE_LOGGED_IN_USER")
public @interface IsLoggedIn {

}
