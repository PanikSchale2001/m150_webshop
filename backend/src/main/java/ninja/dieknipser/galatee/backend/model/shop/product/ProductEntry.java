package ninja.dieknipser.galatee.backend.model.shop.product;

import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import ninja.dieknipser.galatee.backend.model.shop.cart.Cart;

@Entity
public class ProductEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne(fetch = FetchType.EAGER)
	private Product product;
	private int count;

	/**
	 * A db helper property. This doesn't get mapped to the dtos!
	 */
	@ManyToOne
	private Cart cart;

	/**
	 * default constructor
	 */
	public ProductEntry() {
	}

	public ProductEntry(Cart cart, Product product, int count) {
		this.cart = cart;
		this.product = product;
		this.count = count;
	}

	/**
	 * Returns the id in an optional. This reflects that the id can be null if this
	 * object isn't saved in the database. This can be the case when the cart is
	 * saved in memory because for example it is only a temporary cart
	 *
	 * @return the id wrapped in a optional
	 */
	public Optional<Long> getId() {
		if (id == null) {
			return Optional.empty();
		}
		return Optional.of(id);
	}

	public Cart getCart() {
		return cart;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @param count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProductEntry other = (ProductEntry) obj;
		if (count != other.count) {
			return false;
		}
		if (product == null) {
			if (other.product != null) {
				return false;
			}
		} else if (!product.equals(other.product)) {
			return false;
		}
		return true;
	}

}
