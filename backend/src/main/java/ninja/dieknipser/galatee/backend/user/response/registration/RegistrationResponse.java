package ninja.dieknipser.galatee.backend.user.response.registration;

public class RegistrationResponse {
	private RegistrationStatus status;

	/**
	 * @param status
	 */
	public RegistrationResponse(RegistrationStatus status) {
		this.status = status;
	}

	/**
	 * @return the status
	 */
	public RegistrationStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(RegistrationStatus status) {
		this.status = status;
	}

}
