package ninja.dieknipser.galatee.backend.configuration.security;

public class SecurityConstants {
	public static final long EXPIRATION_TIME = 10 * 60 * 60 * 1000; // 10 hours
	public static final String SECRET = "secret";
	public static final String HEADER_STRING = "Authorization";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String SIGN_UP_URL = "/api/user/register";
}
