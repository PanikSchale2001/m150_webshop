package ninja.dieknipser.galatee.backend.manager.shop;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import ninja.dieknipser.galatee.backend.model.shop.cart.Cart;
import ninja.dieknipser.galatee.backend.model.shop.cart.CartService;
import ninja.dieknipser.galatee.backend.trackedlinks.HashGenerator;

@Component
public class CartManager {
	private HashGenerator hashGenerator;
	private Map<String, Cart> carts = new HashMap<>();
	/**
	 * the cart service
	 */
	private CartService cartService;

	public CartManager(HashGenerator hashGenerator, CartService cartService) {
		this.hashGenerator = hashGenerator;
		this.cartService = cartService;
		initFromDb();
	}

	/**
	 * Gets the persistanced carts from the database
	 */
	private void initFromDb() {
		cartService.getAll().forEach(cart -> carts.put(cart.getId(), cart));
	}

	public Cart createCart() {
		Cart cart = new Cart(hashGenerator.generateHash());
		carts.put(cart.getId(), cart);
		return cart;
	}

	public Cart findCart(String id) {
		return carts.get(id);
	}

	/**
	 * Deletes the given cart from the cart
	 *
	 * @param id the id of the cart
	 * @return the removed cart or null if the cart doesn't exist OR the cart is
	 *         persistence
	 */
	public Cart removeCart(String id) {
		Cart cart = findCart(id);
		if (cart != null && cart.isInDatabase()) {
			return null;
		}
		return carts.remove(id);
	}

	/**
	 * Saves the given cart in the database
	 *
	 * @param cart the cart
	 * @return
	 */
	public Cart saveInDatabase(Cart cart) {
		if (cart.getUser().isEmpty()) {
			throw new IllegalStateException("User is empty and trying to save in db");
		}
		Cart newCart = cartService.save(cart);
		carts.put(cart.getId(), newCart);
		return newCart;
	}
}
