package ninja.dieknipser.galatee.backend.search;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ninja.dieknipser.galatee.backend.model.shop.product.Product;
import ninja.dieknipser.galatee.backend.model.shop.product.ProductService;

/**
 * The service used for searching products
 *
 * @author sebi
 *
 */
@Service
public class SearchService {
	/**
	 * the product service
	 */
	private ProductService productService;

	/**
	 * Constructor
	 *
	 * @param productService the product service
	 */
	public SearchService(ProductService productService) {
		this.productService = productService;
	}

	/**
	 * Finds a product with a like term
	 *
	 * @param term     the search term
	 * @param pageable the pageable request
	 * @return a page object
	 * @see Page#getContent()
	 */
	public Page<Product> searchProducts(String term, Pageable pageable) {
		return productService.searchProduct(term, pageable);
	}
}
