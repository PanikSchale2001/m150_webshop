package ninja.dieknipser.galatee.backend.controller.product;

import java.util.List;

import ninja.dieknipser.galatee.backend.model.shop.product.dto.ProductResponseDTO;

public class ProductResponse {
	private  int pagesLeft;
	private List<ProductResponseDTO> productList;

	/**
	 *
	 * @param pagesLeft
	 * @param productList
	 */
	public ProductResponse(int pagesLeft, List<ProductResponseDTO> productList) {
		this.pagesLeft = pagesLeft;
		this.productList = productList;
	}

	/**
	 *
	 * @return pagesLeft
	 */
	public int getPagesLeft() {
		return pagesLeft;
	}

	/**
	 *
	 * @return productList
	 */
	public List<ProductResponseDTO> getProductList() {
		return productList;
	}

	/**
	 *
	 * @param pagesLeft to set
	 */
	public void setPagesLeft(int pagesLeft) {
		this.pagesLeft = pagesLeft;
	}

	/**
	 *
	 * @param productList to set
	 */
	public void setProductList(List<ProductResponseDTO> productList) {
		this.productList = productList;
	}
}
