package ninja.dieknipser.galatee.backend.model.user.dto;

public class ResponseApplicationUserDTO {
	private long id;
	private String name;
	private String email;

	private boolean activated = false;

	/**
	 * Constructor
	 */
	public ResponseApplicationUserDTO() {
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @return the username
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param username the username to set
	 */
	public void setName(String username) {
		this.name = username;
	}

	/**
	 * @return the password
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param password the password to set
	 */
	public void setEmail(String password) {
		this.email = password;
	}

	/**
	 * @return the activated
	 */
	public boolean isActivated() {
		return activated;
	}

	/**
	 * @param activated the activated to set
	 */
	public void setActivated(boolean activated) {
		this.activated = activated;
	}

}
