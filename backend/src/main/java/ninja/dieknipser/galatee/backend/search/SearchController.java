package ninja.dieknipser.galatee.backend.search;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ninja.dieknipser.galatee.backend.model.shop.product.Product;
import ninja.dieknipser.galatee.backend.model.shop.product.dto.ProductResponseDTO;
import ninja.dieknipser.galatee.backend.model.utils.helper.ModelMapperHelper;
import ninja.dieknipser.galatee.backend.search.dto.SearchResultDTO;

/**
 * The controller used for searches
 * @author sebi
 *
 */
@RestController
@RequestMapping("/api/search")
public class SearchController {

	private static final String DEFAULT_PAGE_SIZE = "20";
	/**
	 * The search service
	 */
	private SearchService searchService;

	/**
	 * the modelmapper helper
	 */
	private ModelMapperHelper modelMapperHelper;

	/**
	 * Constructor
	 *
	 * @param searchService     the search service
	 * @param modelMapperHelper the model mapper helper
	 */
	public SearchController(SearchService searchService, ModelMapperHelper modelMapperHelper) {
		this.searchService = searchService;
		this.modelMapperHelper = modelMapperHelper;
	}

	/**
	 * Searches for the given page
	 *
	 * @param searchTerm the term
	 * @param page       the current page
	 * @param pageSize   the size of the page (optional)
	 * @return the seach result
	 */
	@GetMapping("/{term}/{page}")
	public SearchResultDTO search(@PathVariable("term") String searchTerm, @PathVariable("page") int page,
			@RequestParam(name = "pageSize", required = false, defaultValue = DEFAULT_PAGE_SIZE) int pageSize) {
		Page<Product> resultPage = searchService.searchProducts(searchTerm,
				PageRequest.of(page, pageSize));

		List<Product> resultList = resultPage.getContent();
		List<ProductResponseDTO> resultDto = modelMapperHelper.mapAll(resultList, ProductResponseDTO.class);
		return new SearchResultDTO(resultDto, resultPage.getNumber(), resultPage.getTotalPages());
	}

}
