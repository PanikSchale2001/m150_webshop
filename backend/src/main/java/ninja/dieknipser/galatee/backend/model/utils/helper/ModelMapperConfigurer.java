package ninja.dieknipser.galatee.backend.model.utils.helper;

import org.modelmapper.ModelMapper;

/**
 * An interface which is called for configuring the model mapper
 *
 * @author sebi
 * @see ModelMapperHelper
 *
 */
public interface ModelMapperConfigurer {
	/**
	 * Configures the model mapper
	 *
	 * @param modelMapper the model mapper's configuration
	 */
	void configure(ModelMapper modelMapper);
}
