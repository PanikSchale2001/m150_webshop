package ninja.dieknipser.galatee.backend.configuration.applicationconfig;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import ninja.dieknipser.galatee.backend.mail.MailController;

/**
 * The configuration for the {@link MailController}. It can be loaded from the
 * appliation.properties and references the java mail properties file
 *
 * @author sebi
 *
 */
@ConfigurationProperties(prefix = "mail")
public class MailConfig {
	/**
	 * if the mail sending is enabled
	 */
	private boolean enable = false;

	/**
	 * the java mail properties file
	 */
	private List<String> configFile = new ArrayList<>();

	/**
	 * Constructor
	 */
	public MailConfig() {
	}

	/**
	 * If sending mail is enabled
	 *
	 * @return is enabled
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * If sending mail is enabled
	 *
	 * @param enable sets if enabled or not
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	/**
	 * the java mail config file
	 *
	 * @return the file object to the config file
	 */
	public List<String> getConfigFile() {
		return configFile;
	}

	/**
	 * Sets the java mail config file location
	 *
	 * @param configFile the config file
	 */
	public void setConfigFile(List<String> configFile) {
		this.configFile = configFile;
	}

}
