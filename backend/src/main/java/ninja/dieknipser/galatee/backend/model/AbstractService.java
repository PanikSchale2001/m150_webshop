package ninja.dieknipser.galatee.backend.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class AbstractService<REPO extends JpaRepository<T, ID>, T, ID> {
	private REPO repository;

	/**
	 * Constructor
	 *
	 * @param repository the jpa repository
	 */
	public AbstractService(REPO repository) {
		this.repository = repository;
	}

	public Optional<T> findById(ID id) {
		return repository.findById(id);
	}

	public boolean existsById(ID id) {
		return repository.existsById(id);
	}

	public List<T> getAll() {
		return repository.findAll();
	}

	/**
	 * Returns the requested page from the repository
	 * @param page the page (starts with index 0)
	 * @param pageSize the size of the page
	 * @return the page object
	 * @see Page#getContent() 
	 * @see JpaRepository#findAll(Pageable)
	 */
	public Page<T> getAll(int page, int pageSize) {
		return repository.findAll(PageRequest.of(page, pageSize));
	}

	public void delete(T obj) {
		repository.delete(obj);
	}

	public void deleteById(ID id) {
		repository.deleteById(id);
	}


	public T save(T obj) {
		return repository.save(obj);
	}


	protected REPO getRepository() {
		return repository;
	}
}
