package ninja.dieknipser.galatee.backend.model.user.dto;

import javax.validation.constraints.NotNull;

public class LoginApplicationUserDTO {
	@NotNull
	private String email;
	@NotNull
	private String password;

	/**
	 * Constructor
	 */
	public LoginApplicationUserDTO() {
	}

	/**
	 * @return the username
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param username the username to set
	 */
	public void setEmail(String username) {
		this.email = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
