package ninja.dieknipser.galatee.backend.model.shop.cart.dto;

import java.util.ArrayList;
import java.util.List;

import ninja.dieknipser.galatee.backend.model.shop.cart.Cart;
import ninja.dieknipser.galatee.backend.model.shop.product.dto.ProductEntryResponseDTO;

/**
 * A dto for {@link Cart}
 *
 * @author sebi
 *
 */
public class CartResponseDTO {
	private String id;
	private List<ProductEntryResponseDTO> productEntries = new ArrayList<>();
	private double totalAmount;
	private double totalAmountWithTax;

	public CartResponseDTO() {
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the totalAmount
	 */
	public double getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the totalAmountWithTax
	 */
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	/**
	 * @param totalAmountWithTax the totalAmountWithTax to set
	 */
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	/**
	 * @return the productEntries
	 */
	public List<ProductEntryResponseDTO> getProductEntries() {
		return productEntries;
	}

}
