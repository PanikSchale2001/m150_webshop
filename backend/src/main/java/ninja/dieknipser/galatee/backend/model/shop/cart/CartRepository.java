package ninja.dieknipser.galatee.backend.model.shop.cart;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The cart repository for the {@link Cart} table
 *
 * @author sebi
 *
 */
public interface CartRepository extends JpaRepository<Cart, Long> {

}
