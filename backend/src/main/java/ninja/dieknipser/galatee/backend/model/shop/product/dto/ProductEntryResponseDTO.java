package ninja.dieknipser.galatee.backend.model.shop.product.dto;

import ninja.dieknipser.galatee.backend.model.shop.product.ProductEntry;

/**
 * A dto for {@link ProductEntry}
 *
 * @author sebi
 *
 */
public class ProductEntryResponseDTO {
	private ProductResponseDTO product;
	private int count;

	public ProductEntryResponseDTO() {
	}

	/**
	 * @param product
	 * @param count
	 */
	public ProductEntryResponseDTO(ProductResponseDTO product, int count) {
		this.product = product;
		this.count = count;
	}

	/**
	 * @return the product
	 */
	public ProductResponseDTO getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(ProductResponseDTO product) {
		this.product = product;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

}
