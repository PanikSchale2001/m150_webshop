package ninja.dieknipser.galatee.backend.configuration.applicationconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import ninja.dieknipser.galatee.backend.mail.MailController;

/**
 * The configuration for the {@link MailController}. It can be loaded from the
 * appliation.properties and references the java mail properties file
 *
 * @author sebi
 *
 */
@ConfigurationProperties(prefix = "user")
public class UserConfig {
	/**
	 * the path to the password reset mail template
	 */
	private String passwordResetTemplatePath = "@user/password-reset.template";
	/**
	 * the path to the activation mail template
	 */
	private String activationTemplatePath = "@user/activation.template";
	/**
	 * the sender of the mail
	 */
	private String mailSender = "reset-password@galatee.com";

	/**
	 * The subject of the password reset mail
	 */
	private String passwordResetSubject = "Password Reset Request";

	/**
	 * the subject of the account activation mail
	 */
	private String activationSubject = "Account activation";

	/**
	 * how long the link is valid in miliseconds
	 */
	private long linkExpiration = 15 * 60 * 1000; // 15 min default

	/**
	 * Constructor
	 */
	public UserConfig() {
	}

	/**
	 * @return the templatePath
	 */
	public String getPasswordResetTemplatePath() {
		return passwordResetTemplatePath;
	}

	/**
	 * @param templatePath the templatePath to set
	 */
	public void setPasswordResetTemplatePath(String templatePath) {
		this.passwordResetTemplatePath = templatePath;
	}

	/**
	 * @return the activationTemplatePath
	 */
	public String getActivationTemplatePath() {
		return activationTemplatePath;
	}

	/**
	 * @param activationTemplatePath the activationTemplatePath to set
	 */
	public void setActivationTemplatePath(String activationTemplatePath) {
		this.activationTemplatePath = activationTemplatePath;
	}

	/**
	 * @return the mailSender
	 */
	public String getMailSender() {
		return mailSender;
	}

	/**
	 * @param mailSender the mailSender to set
	 */
	public void setMailSender(String mailSender) {
		this.mailSender = mailSender;
	}

	/**
	 * @return the passwordResetSubject
	 */
	public String getPasswordResetSubject() {
		return passwordResetSubject;
	}

	/**
	 * @param passwordResetSubject the passwordResetSubject to set
	 */
	public void setPasswordResetSubject(String passwordResetSubject) {
		this.passwordResetSubject = passwordResetSubject;
	}

	/**
	 * @return the activationSubject
	 */
	public String getActivationSubject() {
		return activationSubject;
	}

	/**
	 * @param activationSubject the activationSubject to set
	 */
	public void setActivationSubject(String activationSubject) {
		this.activationSubject = activationSubject;
	}

	/**
	 * @return the linkExpiration
	 */
	public long getLinkExpiration() {
		return linkExpiration;
	}

	/**
	 * @param linkExpiration the linkExpiration to set
	 */
	public void setLinkExpiration(long linkExpiration) {
		this.linkExpiration = linkExpiration;
	}

}
