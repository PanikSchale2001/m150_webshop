package ninja.dieknipser.galatee.backend.model.shop.image;

import org.springframework.stereotype.Service;

import ninja.dieknipser.galatee.backend.model.AbstractService;

@Service
public class ImageService extends AbstractService<ImageRepository, Image, Long> {

	/**
	 * The base image api path
	 */
	private static final String IMAGE_BASE_PATH = "/api/image/";

	/**
	 * Constructor
	 *
	 * @param repository the jpa repository
	 */
	public ImageService(ImageRepository repository) {
		super(repository);
	}

	/**
	 * Creates the (relative to the host) url of the given image (using
	 * {@link #IMAGE_BASE_PATH}
	 *
	 * @param img the image
	 * @return the url
	 */
	public String getImagePath(Image img) {
		return IMAGE_BASE_PATH + img.getId();
	}
}
