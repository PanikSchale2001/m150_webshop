package ninja.dieknipser.galatee.backend.trackedlinks;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;

/**
 * Manager for tracked links. It manages them and deletes them when they expire.
 * Tracked links <b>are not</b> stored in the database because they are
 * temporary (usually 15 min)
 *
 * @author sebi
 * @param <T> the instance of the {@link LinkModel}
 * @see LinkFactory
 */
public class LinkManager<T extends LinkModel> {
	/**
	 * the logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * the <b>sorted</b> map from all links. It is sorted by the expiration date of
	 * the links
	 */
	private Map<String, T> linkMap = new HashMap<>();

	/**
	 * a list with the tracked links sorted after their expiration time
	 */
	private Set<T> sortedLinkList = new TreeSet<>(sortTrackedLinkModels());
	/**
	 * The thread
	 */
	private Thread thread = null;


	/**
	 * The baseurl used to generated the links
	 */
	private String baseUrl;

	/**
	 * the api url used to generate links
	 */
	private String apiUrl;

	/**
	 * Time in mili seconds between checking the expiration date of the links
	 */
	private volatile long linkTerminationSleep = 60 * 1000;

	/**
	 * The factory
	 */
	private LinkFactory<T> factory;

	/**
	 * the hash generator
	 */
	private HashGenerator hashGenerator;

	/**
	 * Constructor
	 *
	 * @param baseurl the baseurl of the backend to generate links
	 * @param apiUrl  the api url
	 * @param factory the factory
	 */
	public LinkManager(String baseurl, String apiUrl, LinkFactory<T> factory, HashGenerator hashGenerator) {
		this.baseUrl = baseurl;
		this.apiUrl = apiUrl;
		this.factory = factory;
		this.hashGenerator = hashGenerator;
	}

	/**
	 * Generates a new tracked link<br>
	 * This method is syncronized so that the current values of {@link #linkMap} and
	 * {@link #sortedLinkList} are used
	 *
	 *
	 * @param validDuration the duration in miliseconds of how long it is valid
	 * @param user          the current user
	 * @return the link model
	 */
	public synchronized LinkModel generateLink(long validDuration, ApplicationUser user) {
		String hash = hashGenerator.generateHash();
		LocalDateTime current = LocalDateTime.now();
		LocalDateTime expirationDate = current.plus(validDuration, ChronoUnit.MILLIS);
		T link = factory.create(getBaseUrl(), getApiUrl(), hash, current, expirationDate, user);
		synchronized (sortedLinkList) {
			sortedLinkList.add(link);
			linkMap.put(hash, link);
		}
		return link;
	}

	/**
	 * Returns the link from the hash
	 *
	 * @param hash the hash
	 * @return optional which resolves to the link
	 */
	public Optional<T> getLink(String hash) {
		T link = linkMap.get(hash);
		if(link == null) {
			return Optional.empty();
		}
		return Optional.of(link);
	}

	/**
	 * Deactivates the link by removing it. This method is syncronized so the
	 * current values of {@link #linkMap} and {@link #sortedLinkList} are used
	 *
	 * @param hash the hash of the link
	 */
	public void deactivateLink(String hash) {
		synchronized (sortedLinkList) {
			LinkModel removedLink = linkMap.remove(hash);
			logger.info("Deactivate link \"{}\" (link: \"{}\")", hash, removedLink.getUrl());
			sortedLinkList.remove(removedLink);
			removedLink.setExpired(true);
			// set link expired
		}
	}

	/**
	 * The baseurl
	 *
	 * @return the baseurl of the backend
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * the api url
	 *
	 * @return the api url
	 */
	public String getApiUrl() {
		return apiUrl;
	}

	/**
	 * @return the linkTerminationSleep
	 */
	public long getLinkTerminationSleep() {
		return linkTerminationSleep;
	}

	/**
	 * @param linkTerminationSleep the linkTerminationSleep to set
	 */
	public void setLinkTerminationSleep(long linkTerminationSleep) {
		this.linkTerminationSleep = linkTerminationSleep;
	}


	/**
	 * Starts the thread which checks if links are expired
	 */
	public synchronized void start() {
		// if thread is already running -> return
		if (thread != null && thread.isAlive()) {
			return;
		}

		logger.info("Starting the trackedLinkManager with the apiUrl \"{}\"", getApiUrl());

		thread = new Thread(getThreadRunnable());
		thread.start();
	}

	/**
	 * Creates a new runnable instance for checking the expiration date of links. It
	 * goes through all links and checks if they are expired. Because the linkMap is
	 * sorted, it has to check only until it sees the first which isn't expired yet.
	 *
	 * It will delete the expired links.
	 *
	 * @return the runnable for checking if links are expired
	 */
	private Runnable getThreadRunnable() {
		return () -> {
			while (!Thread.currentThread().isInterrupted()) {
				// goes throught all links which are before now and puts them on the
				// toDeleteList. It breaks out of the loop when one is after now, because the
				// map is sorted after the expiration time
				synchronized (sortedLinkList) {
					List<T> toDeleteList = new ArrayList<>();
					for (T currentLink : sortedLinkList) {
						if (currentLink.getExpiresAt().isBefore(LocalDateTime.now())) {
							toDeleteList.add(currentLink);
						} else {
							break;
						}
					}
					// remove link from link model map and list
					toDeleteList.stream().map(T::getHash).forEach(this::deactivateLink);
				}

				try {
					Thread.sleep(getLinkTerminationSleep());
				} catch (InterruptedException e) {
					logger.warn("Link terminator thread was interrupted", e);
					return;
				}
			}
		};
	}

	/**
	 * Stops the thread and waits until it terminated
	 */
	public synchronized void stop() {
		// checks if the thread is actually still running; if not -> return
		if (thread == null || !thread.isAlive()) {
			return;
		}

		logger.info("Stopping TrackedLinkManager");
		thread.interrupt();
		// waits for the thread to die for 1 second and throws a RuntimeException after
		// that
		for (int i = 0; thread.isAlive() && i < 20; i++) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				logger.warn("waitToStop() was interrupted", e);
			}
		}
		// thread still runs after 1s after the interrupt -> throw RuntimeException
		// because the caller can't be expected to handle this error
		if (thread.isAlive()) {
			throw new RuntimeException("Couldn't stop trackedlink expirerer");
		}
	}


	/**
	 * Returns a comparator which checks sorts {@link LinkModel}after their
	 * expiration date
	 *
	 * @return the comparator
	 */
	private static Comparator<LinkModel> sortTrackedLinkModels() {
		return (m1, m2) -> {
			if(m1.getExpiresAt().isEqual(m2.getExpiresAt())) {
				return 0;
			} else if(m1.getExpiresAt().isAfter(m2.getExpiresAt())) {
				return 1;
			} else {
				return -1;
			}
		};
	}

}
