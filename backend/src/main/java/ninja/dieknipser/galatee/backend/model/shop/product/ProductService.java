package ninja.dieknipser.galatee.backend.model.shop.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ninja.dieknipser.galatee.backend.model.AbstractService;

/**
 * The service for {@link Product}
 *
 * @author sebi
 *
 */
@Service
public class ProductService extends AbstractService<ProductRepository, Product, Long> {
	/**
	 * Constructor
	 *
	 * @param repository the jpa repository
	 */
	public ProductService(ProductRepository repository) {
		super(repository);
	}

	/**
	 * Finds a product with a like term
	 *
	 * @param searchTerm the search term
	 * @param pageable   the pageable request
	 * @return a page object
	 * @see Page#getContent()
	 */
	public Page<Product> searchProduct(String searchTerm, Pageable pageable) {
		return getRepository().searchProduct(searchTerm, pageable);
	}

}
