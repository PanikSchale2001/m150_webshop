package ninja.dieknipser.galatee.backend.user.response.registration;

public enum RegistrationStatus {
	SUCCESS, USERNAME_TAKEN, GENERAL_ERROR;
}
