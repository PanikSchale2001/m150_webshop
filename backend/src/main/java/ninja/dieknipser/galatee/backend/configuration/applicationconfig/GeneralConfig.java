package ninja.dieknipser.galatee.backend.configuration.applicationconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * A config class for general config values in application.properties needed by
 * the galatee backend
 *
 * @author sebi
 *
 */
@ConfigurationProperties(prefix = "general"	)
public class GeneralConfig {
	/**
	 * The baseurl of galatee
	 */
	private String baseUrl;

	/**
	 * The general length of hashes
	 */
	private int hashLength = 20;

	private String imgPath ="./img/";

	/**
	 * Constructor
	 */
	public GeneralConfig() {
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the hashLength
	 */
	public int getHashLength() {
		return hashLength;
	}

	/**
	 * @param hashLength the hashLength to set
	 */
	public void setHashLength(int hashLength) {
		this.hashLength = hashLength;
	}

	/**
	 *
	 * @return Img Path
	 */
	public String getImgPath() {
		return imgPath;
	}

	/**
	 *
	 * @param imgPath to set
	 */
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
}
