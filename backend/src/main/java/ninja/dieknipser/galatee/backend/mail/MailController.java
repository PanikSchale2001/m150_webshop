package ninja.dieknipser.galatee.backend.mail;

import java.io.File;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.AsyncResponse;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.config.ConfigLoader;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.samskivert.mustache.Mustache;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.MailConfig;

/**
 * The controller which handels every basic mailing needs. It is used to send
 * activation and password mails
 *
 * @author sebi
 *
 */
@Component
public class MailController {
	/**
	 * logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * the mail config. It is injected in the constructor
	 */
	private MailConfig mailConfig;
	/**
	 * the mailer itself which is build when first used by {@link #getMailer()}
	 */
	private Mailer mailer = null;

	/**
	 * Constructor
	 *
	 * @param mailConfig the mail config
	 */
	public MailController(MailConfig mailConfig) {
		this.mailConfig = mailConfig;
	}

	/**
	 * Sends the given mail to the given address with the given subject
	 *
	 * @param toName         the name of the sender
	 * @param toEmailAddress the email address of the sender
	 * @param from           who sends the mail
	 * @param subject        the subject of the mail
	 * @param mail           the mail itself (not a template)
	 * @return the async response for error handling (An error and info is logged by
	 *         default)
	 */
	public AsyncResponse send(String toName, String toEmailAddress, String from, String subject, String mail) {
		if (!mailConfig.isEnable()) {
			logger.warn(
					"Sending mail is disabled in the configuration (mail.enable=true to enable it). Mail to \"{}\" with the subject \"{}\" is discared",
					from, subject);
			// returns an completed async response that the caller doesn't have to handle
			// null
			return getCompletedAsyncResponse();
		}
		logger.info("Trying to send mail to \"{}\" with the subject \"{}\"", toEmailAddress, subject);
		Email email = EmailBuilder.startingBlank().to(toName, toEmailAddress).withSubject(subject).from(from)
				.withHTMLText(mail).buildEmail();

		Mailer mailer = getMailer();
		AsyncResponse response = mailer.sendMail(email, true);
		response.onException(t -> logger.error("Couldn't send mail to \"{}\"", toEmailAddress, t));
		response.onSuccess(() -> logger.info("Sent mail to \"{}\" with the subject \"{}\"", toEmailAddress, subject));
		return response;
	}

	/**
	 * Sends a {@link MailTemplate} to the given address. The subject and mail
	 * itself is taken from the {@link MailTemplate} and the template is rendered
	 * with the given variables
	 *
	 * @param toName         the recipients name
	 * @param toEmailAddress the recipients email address
	 * @param variables      the variables which should be used when rendering the
	 *                       mail
	 * @param template       the template object itself
	 * @return the async response to track the progress and handle errors
	 */
	public AsyncResponse send(String toName, String toEmailAddress, Map<String, Object> variables,
			MailTemplate template) {
		String renderedMail = Mustache.compiler().compile(template.getTemplate()).execute(variables);
		String renderedSubject = Mustache.compiler().compile(template.getSubject()).execute(variables);
		return send(toName, toEmailAddress, template.getFrom(), renderedSubject, renderedMail);
	}

	/**
	 * Loads the mailer if it wasn't already created and loads the java mail config
	 * from {@link MailConfig#getConfigFile()}) else the already created mailer is
	 * returned. The mailer is stored per {@link MailController} and not static
	 *
	 * @return the Mailer
	 */
	private Mailer getMailer() {
		if(mailer == null) {
			mailConfig.getConfigFile().forEach(path -> {
				File pathFile = new File(path);
				if (!pathFile.exists()) {
					logger.warn("Couldn't load mail config file at \"{}\"", pathFile.getAbsolutePath());
				}
				logger.info("Load mail config at \"{}\"", pathFile.getAbsolutePath());
				ConfigLoader.loadProperties(pathFile, true);
			});
			mailer = MailerBuilder.buildMailer();
		}
		return  mailer;
	}

	/**
	 * Creates a new async response which executes the onSuccess runnable instantly
	 * and returns a completed future
	 *
	 * @return the completed AsyncResponse
	 */
	private AsyncResponse getCompletedAsyncResponse() {
		return new AsyncResponse() {

			@Override
			public void onSuccess(Runnable onSuccess) {
				onSuccess.run();
			}

			@Override
			public void onException(ExceptionConsumer errorHandler) {
			}

			@Override
			public Future<?> getFuture() {
				return CompletableFuture.completedFuture(null);
			}
		};
	}
}
