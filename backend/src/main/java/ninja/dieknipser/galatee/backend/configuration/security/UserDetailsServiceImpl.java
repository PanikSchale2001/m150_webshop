package ninja.dieknipser.galatee.backend.configuration.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;
import ninja.dieknipser.galatee.backend.model.user.UserService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	private UserService userService;

	/**
	 * @param userService
	 */
	public UserDetailsServiceImpl(UserService userService) {
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ApplicationUser user = userService.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException(username));
		return new User(user.getEmail(), user.getPassword(), user.isActivated(), true, true, true,
				getGrantedAuthorities(user));
	}

	private List<GrantedAuthority> getGrantedAuthorities(ApplicationUser user) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_LOGGED_IN_USER"));
		return authorities;
	}

}
