package ninja.dieknipser.galatee.backend.model.shop.image;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import ninja.dieknipser.galatee.backend.model.shop.product.Product;

@Entity
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String path;

    @ManyToOne
    @JoinColumn(name = "product_fk")
    private Product product;

    public Image() {
    }

    /**
     * @param name
     * @param path
     */
    public Image(@NotNull String name, @NotNull String path) {
        this.name = name;
        this.path = path;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    @ManyToOne(optional = false)
    private Product products;

    /**
     *
     * @return product
     */
    public Product getProducts() {
        return products;
    }

    /**
     *
     * @param products to set
     */
    public void setProducts(Product products) {
        this.products = products;
    }
}
