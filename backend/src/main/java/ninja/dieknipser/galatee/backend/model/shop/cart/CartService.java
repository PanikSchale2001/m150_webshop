package ninja.dieknipser.galatee.backend.model.shop.cart;

import org.springframework.stereotype.Service;

import ninja.dieknipser.galatee.backend.model.AbstractService;

/**
 * The service for the cart
 *
 * @author sebi
 *
 */
@Service
public class CartService extends AbstractService<CartRepository, Cart, Long> {

	/**
	 * Constructor
	 *
	 * @param repository the repository
	 */
	public CartService(CartRepository repository) {
		super(repository);
	}


}
