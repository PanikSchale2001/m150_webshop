package ninja.dieknipser.galatee.backend.trackedlinks;

import java.time.LocalDateTime;

import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;

/**
 * A factory for {@link LinkModel}
 *
 * @author sebi
 *
 * @param <T> the subclass
 */
public interface LinkFactory<T extends LinkModel> {
	/**
	 * Creates the tracked link based on these properties
	 *
	 * @param baseUrl   the baseurl
	 * @param apiUrl    the api url
	 * @param hash      the hash
	 * @param createdAt when it is created
	 * @param expiresAt when it expires
	 * @param user      the user
	 * @return the created link model
	 */
	T create(String baseUrl, String apiUrl, String hash, LocalDateTime createdAt,
			LocalDateTime expiresAt, ApplicationUser user);

	/**
	 * The default implementation for the {@link LinkFactory}
	 *
	 * @author sebi
	 *
	 */
	public static class DefaultLinkFactory implements LinkFactory<LinkModel> {

		@Override
		public LinkModel create(String baseUrl, String apiUrl, String hash, LocalDateTime createdAt,
				LocalDateTime expiresAt, ApplicationUser user) {
			return new LinkModel(baseUrl, apiUrl, hash, createdAt, expiresAt, user);
		}

	}
}
