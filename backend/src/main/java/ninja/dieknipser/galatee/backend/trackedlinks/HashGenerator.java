package ninja.dieknipser.galatee.backend.trackedlinks;

import java.security.SecureRandom;

import org.springframework.stereotype.Component;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.GeneralConfig;

/**
 * A component to generate hashes
 *
 * @author sebi
 *
 */
@Component
public class HashGenerator {
	/**
	 * the general config
	 */
	private GeneralConfig generalConfig;
	/**
	 * the random instance for generating the hash
	 */
	private SecureRandom random;

	/**
	 * Constructor
	 *
	 * @param generalConfig the general config
	 */
	public HashGenerator(GeneralConfig generalConfig) {
		this.generalConfig = generalConfig;
		this.random = new SecureRandom();
	}

	/**
	 * Generates a new random string using {@link #random} with the given length.
	 * The string has the following characters in it [a-zA-Z0-9]
	 *
	 * @param length the length
	 * @return the "hash"
	 */
	public String generateHash(int length) {
		int lowLimit = 48; // 0
		int highLimit = 122; // z
		// from https://www.baeldung.com/java-random-string
		return random.ints(lowLimit, highLimit + 1)
				.filter(i -> (i >= 48 && i <= 57) || (i >= 65 && i <= 90) || (i >= 97 && i <= 122)).limit(length)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
	}

	/**
	 * Generates a new random string using {@link #random} with the given length.
	 * The string has the following characters in it [a-zA-Z0-9]. The length is
	 * taken from {@link GeneralConfig#getHashLength()}
	 *
	 * @return the "hash"
	 * @see #generateHash(int)
	 */
	public String generateHash() {
		return generateHash(generalConfig.getHashLength());
	}
}
