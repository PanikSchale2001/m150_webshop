package ninja.dieknipser.galatee.backend.controller.shop;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.GeneralConfig;
import ninja.dieknipser.galatee.backend.model.shop.image.Image;
import ninja.dieknipser.galatee.backend.model.shop.image.ImageService;
import ninja.dieknipser.galatee.backend.model.shop.product.ProductService;

@RestController
public class ImageController {
	/**
	 * the logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	private ImageService imageService;
	private ProductService productService;
	private GeneralConfig generalConfig;

	/**
	 *
	 * @param imageService
	 * @param productService
	 * @param generalConfig
	 */
	public ImageController(ImageService imageService, ProductService productService, GeneralConfig generalConfig) {
		this.imageService = imageService;
		this.productService = productService;
		this.generalConfig = generalConfig;
	}

	/**
	 * Get image with the imageId
	 *
	 * @param imageId to find img
	 * @return img as Array of byte
	 * @throws IOException
	 */
	@GetMapping("/api/image/{imageId}")
	public ResponseEntity<byte[]> getImage(@PathVariable("imageId") long imageId) throws IOException {
		imageService.findById(imageId);
		Optional<Image> image = imageService.findById(imageId);
		if(!image.isPresent()) {
			return getDefaultImage();
		}
		File file = new File(generalConfig.getImgPath() + image.get().getPath());
		if (!file.exists()) {
			return getDefaultImage();
		}
		return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(Files.readAllBytes(file.toPath()));
	}

	/**
	 * Reads the no-img.png from the classpath and returns it as a ResponseEntitiy.
	 * This function is used as a helper function for {@link #getImage(long)}
	 *
	 * @return the response entity with the default image
	 * @throws ResponseStatusException if the image couldn't be found
	 */
	private ResponseEntity<byte[]> getDefaultImage() {
		InputStream in = getClass().getResourceAsStream("./no-img.png");
		try {
			byte[] bytes = in.readAllBytes();
			return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(bytes);
		} catch (IOException e) {
			logger.error("Default image not found!");
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "No Default Image");
		}
	}
}
