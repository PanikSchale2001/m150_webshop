package ninja.dieknipser.galatee.backend.controller.product;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import ninja.dieknipser.galatee.backend.model.shop.product.Product;
import ninja.dieknipser.galatee.backend.model.shop.product.ProductService;
import ninja.dieknipser.galatee.backend.model.shop.product.dto.ProductResponseDTO;
import ninja.dieknipser.galatee.backend.model.utils.helper.ModelMapperHelper;

@RestController
public class ProductController {

	private  ProductService productService;

	/**
	 * the model mapper
	 */
	private ModelMapperHelper modelMapperHelper;

	/**
	 *
	 * @param productService
	 * @param modelMapper
	 */
	public ProductController(ProductService productService, ModelMapperHelper modelMapper) {
		this.productService = productService;
		this.modelMapperHelper = modelMapper;
	}

	/**
	 * get Product for the page
	 * @param page to get products
	 * @return products
	 */
	@GetMapping("/api/products/{page}")
	public Object getProduct(@PathVariable("page") int page) {
		Page<Product> productPage = productService.getAll(page, 24);
		List<ProductResponseDTO> productList = productPage.map(p -> modelMapperHelper.map(p, ProductResponseDTO.class))
				.getContent();
		return new ProductResponse(productPage.getTotalPages() - page - 1, productList);
	}

	/**
	 * get products by productId
	 * @param id to get Product
	 * @return products
	 */
	@GetMapping("/api/product/{productId}")
	public ProductResponseDTO getProduct(@PathVariable("productId") long id) {
		Optional<Product> product = productService.findById(id);
		return product.map(p -> modelMapperHelper.map(p, ProductResponseDTO.class))
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Product Not Found"));
	}

}
