package ninja.dieknipser.galatee.backend.model.shop.product;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * The repo for products
 *
 * @author sebi
 *
 */
public interface ProductRepository extends JpaRepository<Product, Long>{
	/**
	 * Finds a product with a like term
	 *
	 * @param searchTerm the search term
	 * @param pageable   the pageable request
	 * @return a page object
	 * @see Page#getContent()
	 */
	@Query("SELECT p FROM Product p WHERE p.name LIKE %:searchTerm% OR p.description LIKE %:searchTerm%")
	public Page<Product> searchProduct(@Param("searchTerm") String searchTerm, Pageable pageable);
}

