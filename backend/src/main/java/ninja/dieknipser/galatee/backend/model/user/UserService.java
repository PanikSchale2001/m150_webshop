package ninja.dieknipser.galatee.backend.model.user;

import java.security.Principal;
import java.util.Optional;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ninja.dieknipser.galatee.backend.model.AbstractService;

/**
 * A service with helper functions for the {@link ApplicationUser}
 *
 * @author sebi
 *
 */
@Service
public class UserService extends AbstractService<ApplicationUserRepository, ApplicationUser, Long> {

	/**
	 * Constructor
	 *
	 * @param repository the repo
	 */
	public UserService(ApplicationUserRepository repository) {
		super(repository);
	}

	/**
	 * Finds the user with the given email
	 *
	 * @param email the email
	 * @return the user
	 */
	public Optional<ApplicationUser> findByEmail(String email) {
		return getRepository().findByEmail(email);
	}

	/**
	 * Returns the application user from the given principal (which can be null)
	 *
	 * @param principal the principal
	 * @return the application user
	 */
	public Optional<ApplicationUser> fromPrincipal(Principal principal) {
		if (principal == null) {
			return Optional.empty();
		}
		String username = principal.getName();
		return findByEmail(username);
	}

	/**
	 * Returns the current user in an optional or an empty optional if no user is
	 * logged in
	 *
	 * @return the user
	 */
	public Optional<ApplicationUser> getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null) {
			return Optional.empty();
		}
		return findByEmail(auth.getName());
	}
}
