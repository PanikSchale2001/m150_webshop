package ninja.dieknipser.galatee.backend.trackedlinks;

import org.springframework.stereotype.Component;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.GeneralConfig;

/**
 * A component which can be injected to create a new {@link LinkManager} with
 * settings from {@link GeneralConfig}
 *
 * @author sebi
 *
 */
@Component
public class LinkManagerFactoryComponent {
	/**
	 * the general config
	 */
	private GeneralConfig generalConfig;

	/**
	 * the hash generator
	 */
	private HashGenerator hashGenerator;

	/**
	 * Constructor
	 *
	 * @param generalConfig the general config
	 * @param hashGenerator the hash generator
	 */
	public LinkManagerFactoryComponent(GeneralConfig generalConfig, HashGenerator hashGenerator) {
		this.generalConfig = generalConfig;
		this.hashGenerator = hashGenerator;
	}

	/**
	 * Creates a new LinkManager from the apiUrl and the factory. The baseurl is
	 * taken from {@link GeneralConfig}. The manager is already started.
	 *
	 * @param <T>     the {@link LinkModel} type
	 * @param apiUrl  the api url
	 * @param factory the factory
	 * @return the link manager
	 */
	public <T extends LinkModel> LinkManager<T> createLinkManager(String apiUrl, LinkFactory<T> factory) {
		LinkManager<T> manager = new LinkManager<>(generalConfig.getBaseUrl(), apiUrl, factory, hashGenerator);
		manager.start();
		return manager;
	}

	/**
	 * Creates a new LinkManager from the apiUrl. The baseurl is taken from
	 * {@link GeneralConfig}
	 *
	 * @param apiUrl the api url
	 * @return the link manager
	 */
	public LinkManager<LinkModel> createLinkManager(String apiUrl) {
		return createLinkManager(apiUrl, new LinkFactory.DefaultLinkFactory());
	}
}
