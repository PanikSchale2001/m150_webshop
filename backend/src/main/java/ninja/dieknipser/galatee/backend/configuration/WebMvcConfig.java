package ninja.dieknipser.galatee.backend.configuration;

import java.util.List;

import javax.persistence.EntityManager;

import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;

import ninja.dieknipser.galatee.backend.model.utils.DTOModelMapper;

/**
 * Configurer for spring web mvc
 *
 * @author sebi
 *
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	private ApplicationContext applicationContext;
	private EntityManager entityManager;

	private ModelMapper modelMapper;

	/**
	 * Constructor
	 *
	 * @param applicationContext the app context
	 * @param entityManager      the hibernate entitiy manager
	 */
	public WebMvcConfig(ApplicationContext applicationContext, EntityManager entityManager, ModelMapper modelMapper) {
		this.applicationContext = applicationContext;
		this.entityManager = entityManager;
		this.modelMapper = modelMapper;
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().applicationContext(applicationContext).build();
		argumentResolvers.add(new DTOModelMapper(objectMapper, entityManager, modelMapper));
	}

	@Override
	public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> handlers) {
	}
}
