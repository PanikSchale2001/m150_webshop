package ninja.dieknipser.galatee.backend.model.utils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Collections;

import javax.persistence.EntityManager;
import javax.persistence.Id;

import org.modelmapper.ModelMapper;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Processes the {@link DTO} annotation. Source:
 * https://auth0.com/blog/automatically-mapping-dto-to-entity-on-spring-boot-apis/
 *
 * @author sebi
 *
 */
public class DTOModelMapper extends RequestResponseBodyMethodProcessor {
	private ModelMapper modelMapper;

	private EntityManager entityManager;

	public DTOModelMapper(ObjectMapper objectMapper, EntityManager entityManager, ModelMapper modelMapper) {
		super(Collections.singletonList(new MappingJackson2HttpMessageConverter(objectMapper)));
		this.entityManager = entityManager;
		this.modelMapper = modelMapper;
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(DTO.class);
	}

	@Override
	public boolean supportsReturnType(MethodParameter returnType) {
		return returnType.hasParameterAnnotation(DTO.class);
	}

	@Override
	protected void validateIfApplicable(WebDataBinder binder, MethodParameter parameter) {
		binder.validate();
		super.validateIfApplicable(binder, parameter);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		Object dto = super.resolveArgument(parameter, mavContainer, webRequest, binderFactory);
		if (dto == null) {
			return null;
		}
		Object id = getEntityId(dto);
		if (id == null) {
			return modelMapper.map(dto, parameter.getParameterType());
		} else {
			Object persistedObject = entityManager.find(parameter.getParameterType(), id);
			if (persistedObject == null) {
				throw new RuntimeException(new InvalidIdException(id));
			}
			modelMapper.map(dto, persistedObject);
			return persistedObject;
		}
	}


	@Override
	protected <T> Object readWithMessageConverters(HttpInputMessage inputMessage, MethodParameter parameter,
			Type targetType) throws IOException, HttpMediaTypeNotSupportedException, HttpMessageNotReadableException {
		for (Annotation ann : parameter.getParameterAnnotations()) {
			DTO dtoType = AnnotationUtils.getAnnotation(ann, DTO.class);
			if (dtoType != null) {
				return super.readWithMessageConverters(inputMessage, parameter, dtoType.value());
			}
		}
		// shouldn't happend because in supportsParameter it looks for the DTO
		// Annotation
		throw new RuntimeException("No DTO Annotation were found");
	}


	/**
	 * Looks for the ID annotation on the dto. If found, the value is returned, else
	 * null is returned
	 *
	 * @param dto the dto
	 * @return the value of the id field or null if no id field was found
	 */
	private Object getEntityId(Object dto) {
		for (Field field : dto.getClass().getDeclaredFields()) {
			if (field.getAnnotation(Id.class) != null) {
				field.setAccessible(true);
				try {
					return field.get(dto);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new RuntimeException(
							"Couldn't access the id \"" + dto.getClass().getName() + "." + field.getName() + "\"", e);
				}
			}
		}
		return null;
	}

}
