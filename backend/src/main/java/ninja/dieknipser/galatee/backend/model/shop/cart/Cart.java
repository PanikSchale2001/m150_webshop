package ninja.dieknipser.galatee.backend.model.shop.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import ninja.dieknipser.galatee.backend.model.shop.product.Product;
import ninja.dieknipser.galatee.backend.model.shop.product.ProductEntry;
import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;

@Entity
public class Cart {
	@Id
	private String id;
	@OneToMany(cascade = CascadeType.ALL, targetEntity = ProductEntry.class, mappedBy = "cart", fetch = FetchType.EAGER, orphanRemoval = true)
	private List<ProductEntry> productEntries = new ArrayList<>();
	private double totalAmount;


	/**
	 * the total amount with sales tax
	 */
	private double totalAmountWithTax;

	@OneToOne
	private ApplicationUser user;

	/**
	 * Default constructor
	 */
	public Cart() {
	}

	public Cart(String id) {
		this.id = id;
	}

	/**
	 * @return the ProductsEntries
	 */
	public List<ProductEntry> getProducts() {
		return productEntries;
	}

	/**
	 * @return the total Amount
	 */
	public double getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @return the totalAmountWithTax
	 */
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	/**
	 * Returns the id
	 *
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns the user or an empty optional if non is set. Also if no user is set
	 * this means, that the cart isn't in the database
	 *
	 * @return the user
	 */
	public Optional<ApplicationUser> getUser() {
		if (user == null) {
			return Optional.empty();
		}
		return Optional.of(user);
	}

	/**
	 * Sets the user
	 *
	 * @param user the new user
	 */
	public void setUser(ApplicationUser user) {
		this.user = user;
	}

	/**
	 * Returns if this cart is saved in the database
	 *
	 * @return if persistance
	 */
	public boolean isInDatabase() {
		return getUser().isPresent();
	}

	/**
	 * add a product to the cart or count up if already exist.
	 *
	 * @param product to add
	 */
	public void addProductToCart(Product product, int count) {

		boolean foundProductEntry = false;
		for (ProductEntry productEntry : productEntries) {
			if (productEntry.getProduct().equals(product)) {
				productEntry.setCount(productEntry.getCount() + count);
				foundProductEntry = true;
				break;
			}
		}
		if (count > 0 && !foundProductEntry) {
			productEntries.add(new ProductEntry(this, product, count));
		}
		calculateTotalAmount();
	}

	/**
	 * Deletes the product entry with the product with the given product id. It will
	 * silently fail, if no product entry was found
	 *
	 * @param productId the product id to look for
	 */
	public void deleteProduct(long productId) {
		productEntries.stream()
		.filter(entry -> entry.getProduct().getId() == productId)
		.findAny()
		.ifPresent(productEntries::remove);
		calculateTotalAmount();
	}

	/**
	 * calculate the Total Amount of the Cart.
	 */
	private void calculateTotalAmount() {
		double tempTotalAmount = 0;
		double tempTotalAmountWithTax = 0;
		for (ProductEntry productEntry : productEntries) {
			tempTotalAmount += productEntry.getProduct().getPrice() * productEntry.getCount();
			tempTotalAmountWithTax += productEntry.getProduct().getPriceWithTax() * productEntry.getCount();
		}
		this.totalAmount = tempTotalAmount;
		this.totalAmountWithTax = tempTotalAmountWithTax;
	}


	/**
	 * Merges the given cart into this cart
	 *
	 * @param cart the cart to merge into this cart
	 */
	public void merge(Cart cart) {
		// stops merging a cart with itself
		if (getId().equals(cart.getId())) {
			return;
		}
		for (ProductEntry e : cart.getProducts()) {
			addProductToCart(e.getProduct(), e.getCount());
		}
	}
}
