package ninja.dieknipser.galatee.backend.model.user.dto;

import javax.validation.constraints.NotNull;

import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;

/**
 * A dto of {@link ApplicationUser} with just the email address
 *
 * @author sebi
 *
 */
public class EmailApplicationUserDto {
	/**
	 * the email of the user
	 */
	@NotNull
	private String email;

	/**
	 * Constructor
	 */
	public EmailApplicationUserDto() {
	}

	/**
	 * Constructor
	 *
	 * @param email the email
	 */
	public EmailApplicationUserDto(@NotNull String email) {
		this.email = email;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
