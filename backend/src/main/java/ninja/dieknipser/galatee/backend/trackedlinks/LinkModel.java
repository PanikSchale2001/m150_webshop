package ninja.dieknipser.galatee.backend.trackedlinks;

import java.time.LocalDateTime;

import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;

/**
 * The model class for tracked links
 *
 * @author sebi
 *
 */
public class LinkModel {
	/**
	 * the base url
	 */
	private String baseUrl;

	/**
	 * the url of the api (without the baseurl)
	 */
	private String apiUrl;
	/**
	 * The hash of the link
	 */
	private String hash;
	/**
	 * when the link was created
	 */
	private LocalDateTime createdAt;

	/**
	 * When the link expires
	 */
	private LocalDateTime expiresAt;

	/**
	 * the user who created the tracked link
	 */
	private ApplicationUser user;

	/**
	 * if the link is expired. This value is set by the {@link LinkManager}
	 * managing this model
	 */
	private boolean expired = false;

	/**
	 * Constructor
	 */
	public LinkModel() {
	}


	/**
	 * Constructor
	 *
	 * @param baseUrl   the baseurl
	 * @param apiUrl    the url to the api from the baseurl (so without the baseurl)
	 * @param hash      the hash
	 * @param createdAt when it was created
	 * @param expiresAt when it expires
	 * @param user      the user who created to link
	 */
	public LinkModel(String baseUrl, String apiUrl, String hash, LocalDateTime createdAt,
			LocalDateTime expiresAt, ApplicationUser user) {
		this.baseUrl = baseUrl;
		this.apiUrl = apiUrl;
		this.hash = hash;
		this.createdAt = createdAt;
		this.expiresAt = expiresAt;
		this.user = user;
	}

	/**
	 * Returns the base url
	 *
	 * @return the base url
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * Returns the api url which is relative to the base url and points to the api
	 * endpoint for clicking the tracked links
	 *
	 * @return the api url
	 */
	public String getApiUrl() {
		return apiUrl;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * Returns the full url consisting of the baseurl, the api url and the hash
	 *
	 * @return the full url
	 * @see #getBaseUrl()
	 * @see #getHash()
	 */
	public String getUrl() {
		return getBaseUrl() + getApiUrl() + getHash();
	}

	/**
	 * Returns the relative url with out the baseurl.
	 *
	 * @return the ralative url
	 */
	public String getRelativeUrl() {
		return getApiUrl() + getHash();
	}


	/**
	 * @return the createdAt
	 */
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	/**
	 * @return the expiresAt
	 */
	public LocalDateTime getExpiresAt() {
		return expiresAt;
	}

	/**
	 * @param expiresAt the expiresAt to set
	 */
	public void setExpiresAt(LocalDateTime expiresAt) {
		this.expiresAt = expiresAt;
	}

	/**
	 * Returns the user who created this link
	 *
	 * @return the user
	 */
	public ApplicationUser getUser() {
		return user;
	}

	/**
	 * Returns if the link is expired or not.
	 *
	 * @return if the link is expired
	 */
	public boolean isExpired() {
		return expired;
	}

	/**
	 * Sets the link to expired (or not expired)
	 *
	 * @param expired if the link is expired or not
	 */
	protected void setExpired(boolean expired) {
		this.expired = expired;
	}
}
