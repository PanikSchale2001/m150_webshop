package ninja.dieknipser.galatee.backend.model.shop.product.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;

import ninja.dieknipser.galatee.backend.model.shop.image.Image;
import ninja.dieknipser.galatee.backend.model.shop.image.ImageService;
import ninja.dieknipser.galatee.backend.model.shop.product.Product;
import ninja.dieknipser.galatee.backend.model.utils.helper.ModelMapperConfiguration;
import ninja.dieknipser.galatee.backend.model.utils.helper.ModelMapperConfigurer;

/**
 * Configuresx the model mapper to convert from {@link Product} to
 * {@link ProductResponseDTO}
 *
 * @author sebi
 *
 */
@ModelMapperConfiguration
public class ProductModelMapperConfigurer implements ModelMapperConfigurer {
	/**
	 * the image service
	 */
	private ImageService imageService;

	/**
	 * Constructor
	 *
	 * @param imageServe the image service
	 */
	public ProductModelMapperConfigurer(ImageService imageServe) {
		this.imageService = imageServe;
	}

	@Override
	public void configure(ModelMapper modelMapper) {
		modelMapper.createTypeMap(Product.class, ProductResponseDTO.class).addMappings(mapping -> {
			Converter<List<Image>, List<String>> imageListConverter = ctx -> ctx.getSource()
					.stream()
					.map(imageService::getImagePath)
					.collect(Collectors.toList());
			mapping.using(imageListConverter).map(Product::getImages, ProductResponseDTO::setImages);
		});
	}

}
