package ninja.dieknipser.galatee.backend.model.utils;

/**
 * An exception which is thrown (by the {@link DTOModelMapper}) when an id isn't
 * found
 *
 * @author sebi
 *
 */
public class InvalidIdException extends Exception {

	/**
	 * Constructor
	 *
	 * @param id the invalid id
	 */
	public InvalidIdException(Object id) {
		super("Invalid ID: " + id);
	}

}
