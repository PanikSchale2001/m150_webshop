package ninja.dieknipser.galatee.backend.configuration;

import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration.AccessLevel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.GeneralConfig;

/**
 * A class which defines common beans used through-out the application
 *
 * @author sebi
 *
 */
@Configuration
public class SpringBeans {
	/**
	 * the general config
	 */
	private GeneralConfig generalConfig;

	/**
	 * Constructor
	 *
	 * @param generalConfig the general config
	 */
	public SpringBeans(GeneralConfig generalConfig) {
		this.generalConfig = generalConfig;
	}

	/**
	 * The bcrypt password encoder
	 *
	 * @return password encoder
	 */
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * @return the model mapper used globally
	 */
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setFieldMatchingEnabled(true).setFieldAccessLevel(AccessLevel.PRIVATE);
		return modelMapper;
	}



}
