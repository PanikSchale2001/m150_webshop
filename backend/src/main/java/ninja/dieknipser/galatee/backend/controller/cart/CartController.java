package ninja.dieknipser.galatee.backend.controller.cart;

import java.security.Principal;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import ninja.dieknipser.galatee.backend.configuration.security.annotation.IsLoggedIn;
import ninja.dieknipser.galatee.backend.manager.shop.CartManager;
import ninja.dieknipser.galatee.backend.model.shop.cart.Cart;
import ninja.dieknipser.galatee.backend.model.shop.cart.CartService;
import ninja.dieknipser.galatee.backend.model.shop.cart.dto.CartResponseDTO;
import ninja.dieknipser.galatee.backend.model.shop.product.ProductRepository;
import ninja.dieknipser.galatee.backend.model.shop.product.ProductService;
import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;
import ninja.dieknipser.galatee.backend.model.user.UserService;
import ninja.dieknipser.galatee.backend.model.utils.helper.ModelMapperHelper;

@RestController
public class CartController {

	/**
	 * the cart manager
	 */
	private CartManager cartManager;
	/**
	 * the cart service
	 */
	private CartService cartService;
	private ProductService productService;
	private ProductRepository productRepository;

	/**
	 * The user service
	 */
	private UserService userService;

	/**
	 * the model mapper helper
	 */
	private ModelMapperHelper modelMapperHelper;

	/**
	 * Constructor
	 *
	 * @param cartManager
	 * @param productService
	 * @param modelMapperHelper
	 * @param userService
	 */
	public CartController(CartManager cartManager, CartService cartService, ProductService productService, ModelMapperHelper modelMapperHelper,
			UserService userService) {
		this.cartManager = cartManager;
		this.cartService = cartService;
		this.productService = productService;
		this.modelMapperHelper = modelMapperHelper;
		this.userService = userService;
	}

	/**
	 * get the cart by id
	 * @param id to get cart
	 * @return a cart by id
	 */
	@GetMapping("/api/cart/{id}")
	public CartResponseDTO getCart(@PathVariable("id") String id) {
		Cart cart = null;
		if (!"".equals(id)) {
			cart = cartManager.findCart(id);
		}
		if (cart == null) {
			cart = cartManager.createCart();
		}
		return modelMapperHelper.map(cart, CartResponseDTO.class);
	}

	@GetMapping("/api/cart/userCart")
	@IsLoggedIn
	public CartResponseDTO getCart(Principal principal) {
		Cart cart = getCartFromUserOrCreate(principal);
		return modelMapperHelper.map(cart, CartResponseDTO.class);
	}

	/**
	 * Adds a product to the cart with the given id
	 *
	 * @param cartId     the cart id
	 * @param productId  the product id to add
	 * @param addCounter how many
	 * @return the updated cart dto
	 */
	@PostMapping("/api/cart/{cart}/add/{product}/{addCounter}")
	public CartResponseDTO addProductToCard(@PathVariable("cart") String cartId,
			@PathVariable("product") Long productId, @PathVariable("addCounter") int addCounter) {
		Cart cart = cartManager.findCart(cartId);
		if (cart == null) {
			cart = cartManager.createCart();
		}
		if (!productService.existsById(productId) || addCounter == 0) {
			return modelMapperHelper.map(cart, CartResponseDTO.class);
		}

		cart.addProductToCart(productService.findById(productId).get(), addCounter);
		if (cart.isInDatabase()) {
			cartManager.saveInDatabase(cart);
		}

		return modelMapperHelper.map(cart, CartResponseDTO.class);
	}


	/**
	 * Removes a product from the cart with the given id
	 *
	 * @param cartId  the cart id
	 * @param product the product id
	 * @return the updated cart dto
	 */
	@DeleteMapping("/api/cart/{cart}/remove/{product}")
	public CartResponseDTO removeProductFromCart(@PathVariable("cart") String cartId,
			@PathVariable("product") Long product) {
		Cart cart = cartManager.findCart(cartId);
		if (cart == null) {
			cart = cartManager.createCart();
		}
		cart.deleteProduct(product);
		if (cart.isInDatabase()) {
			cart = cartManager.saveInDatabase(cart);
		}

		return modelMapperHelper.map(cart, CartResponseDTO.class);
	}


	/**
	 * Attaches the given cart to the given user. This saves the cart to the
	 * database and attaches it to the users account. If the user already has a cart
	 * attached to him/her, then the two carts get merged <br/>
	 *
	 * This obviously needs a logged in user
	 *
	 * @param cartId    the cart id
	 * @param principal the principal to add it to
	 * @return
	 */
	@PostMapping("/api/cart/{cart}/attachToUser")
	@IsLoggedIn
	public CartResponseDTO attachToUser(@PathVariable("cart") String cartId, Principal principal) {
		Cart cart = cartManager.findCart(cartId);
		if (cart == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cart Not Found");
		}

		ApplicationUser user = userService.findByEmail(principal.getName())
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "User Not Found"));

		// if the user already has a cart, then merge the two carts
		Cart newUserCart = user.getCart().map(userCart -> {
			userCart.merge(cart);
			return userCart;
		}).orElse(cart);
		newUserCart.setUser(user);
		newUserCart = cartManager.saveInDatabase(newUserCart);
		user.setCart(newUserCart);
		userService.save(user);

		// deletes the old cart
		cartManager.removeCart(cartId);
		return modelMapperHelper.map(newUserCart, CartResponseDTO.class);
	}

	/**
	 * clear the cart
	 *
	 * @param cartId to get the cart
	 * @param principal to get the principal
	 * @return a cart
	 */
	@PostMapping("/api/cart/{cart}/clear")
	public CartResponseDTO clearCart(@PathVariable("cart") String cartId, Principal principal) {
		Cart oldCart = cartManager.findCart(cartId);
		Cart newCart = cartManager.createCart();
		if (oldCart.isInDatabase()) {
			ApplicationUser user = userService.fromPrincipal(principal)
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.FORBIDDEN, "Not Logged In"));
			newCart.setUser(user);
			newCart = cartManager.saveInDatabase(newCart);
			user.setCart(newCart);
			userService.save(user);
		}
		cartManager.removeCart(cartId);
		return modelMapperHelper.map(newCart, CartResponseDTO.class);
	}

	/**
	 * Copies a cart into a new (temporary) cart. It only clones(!) the product
	 * entries not the user or anything else. The usecase is if the user loggs
	 * him/her self out that the cart changes and doesn't stay connected
	 *
	 * @param cartId the cart id
	 * @return the cloned cart
	 */
	@PostMapping("/api/cart/{cart}/copyCart")
	public CartResponseDTO copyCart(@PathVariable("cart") String cartId) {
		Cart cart = cartManager.findCart(cartId);
		if (cart == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cart Not Found");
		}
		Cart newCart = cartManager.createCart();
		cart.getProducts().forEach(entry -> newCart.addProductToCart(entry.getProduct(), entry.getCount()));
		return modelMapperHelper.map(newCart, CartResponseDTO.class);
	}

	/**
	 * Returns the cart of the given principal or creates a new one and sets it as
	 * the users cart
	 *
	 * @param principal the principal/user
	 * @return the cart of the user
	 * @throws ResponseStatusException
	 */
	private Cart getCartFromUserOrCreate(Principal principal) throws ResponseStatusException {
		ApplicationUser user = userService.fromPrincipal(principal)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "No Logged In User"));
		return user.getCart().orElseGet(() -> {
			// create new cart and set it to the user
			Cart newCart = cartManager.createCart();
			newCart.setUser(user);
			newCart = cartManager.saveInDatabase(newCart);
			user.setCart(newCart);
			userService.save(user);
			return newCart;
		});
	}
}
