package ninja.dieknipser.galatee.backend.model.shop.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import ninja.dieknipser.galatee.backend.model.shop.image.Image;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	private String name;

	@NotNull
	private String description;

	@NotNull
	private double price;

	/**
	 * The sales tax on top of the price
	 */
	private double salesTax;
	private double weight;

	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
	private List<Image> images = new ArrayList<>();

	public Product() {
	}

	/**
	 * @param name
	 * @param description
	 * @param price
	 * @param weight
	 */
	public Product(@NotNull String name, @NotNull String description, @NotNull double price, double salesTax,
			double weight) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.salesTax = salesTax;
		this.weight = weight;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the descriptoin
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the Price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @return the price with the {@link #salesTax} on top of it
	 */
	public double getPriceWithTax() {
		return getPrice() + getPrice() * getSalesTax();
	}

	/**
	 * The sales tax where 1 is 100% (0.32 would be +32% on top of the price)
	 *
	 * @return returns the sales tax
	 */
	public double getSalesTax() {
		return salesTax;
	}
	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * @return the Images
	 */
	public List<Image> getImages() {
		return images;
	}

	/**
	 * @param name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Sets the sales tax (1 = 100%, 0.32 = +32%)
	 *
	 * @param salesTax the sales tax
	 */
	public void setSalesTax(double salesTax) {
		this.salesTax = salesTax;
	}

	/**
	 * @param weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}

	/**
	 * @param images to set
	 */
	public void setImages(List<Image> images) {
		this.images = images;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Product other = (Product) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
