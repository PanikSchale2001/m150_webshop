package ninja.dieknipser.galatee.backend.user;

import java.io.IOException;
import java.security.Principal;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.UserConfig;
import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;
import ninja.dieknipser.galatee.backend.model.user.UserService;
import ninja.dieknipser.galatee.backend.model.user.dto.EmailApplicationUserDto;
import ninja.dieknipser.galatee.backend.model.user.dto.RegisterApplicationUserDTO;
import ninja.dieknipser.galatee.backend.model.user.dto.ResponseApplicationUserDTO;
import ninja.dieknipser.galatee.backend.model.utils.DTO;
import ninja.dieknipser.galatee.backend.trackedlinks.LinkManager;
import ninja.dieknipser.galatee.backend.trackedlinks.LinkManagerFactoryComponent;
import ninja.dieknipser.galatee.backend.trackedlinks.LinkModel;
import ninja.dieknipser.galatee.backend.user.response.PasswordResetObj;
import ninja.dieknipser.galatee.backend.user.response.registration.RegistrationResponse;
import ninja.dieknipser.galatee.backend.user.response.registration.RegistrationStatus;

/**
 * the controller for user related api endpoints
 * @author sebi
 *
 */
@RestController
@RequestMapping("/api/user")
public class UserController {
	/**
	 * the api link for the password reset links
	 */
	public final static String PASSWORD_RESET_API_LINK = "/#/register/resetPassword/";
	/**
	 * the api link for the activation links
	 */
	private static final String ACTIVATION_API_LINK = "/#/register/activation/";

	/**
	 * logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * the user service to interact with the user table
	 */
	private UserService userService;
	/**
	 * the password encrypter
	 */
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	/**
	 * the modelmapper instance to map to and from dtos
	 */
	private ModelMapper modelMapper;


	/**
	 * the user config
	 */
	private UserConfig userConfig;

	/**
	 * the password reset mailer sender
	 */
	private PasswordResetMailSender passwordResetMailSender;

	/**
	 * the activation mail sender
	 */
	private ActivationMailSender activationMailSender;
	/**
	 * the manager for password reset links
	 */
	private LinkManager<LinkModel> resetPasswordLinkManager;

	/**
	 * the manager for the activation links
	 */
	private LinkManager<LinkModel> activationLinkManager;

	/**
	 * Constructor
	 *
	 * @param userService             the user service
	 * @param bCryptPasswordEncoder   the password encoder
	 * @param modelMapper             the model mapper
	 * @param userConfig              the user config
	 * @param linkManagerFactory      the link manager factory
	 * @param passwordResetMailSender the password reset mailer
	 * @param activationMailSender    the activation mail sender
	 */
	public UserController(UserService userService,
			BCryptPasswordEncoder bCryptPasswordEncoder, ModelMapper modelMapper, UserConfig userConfig,
			LinkManagerFactoryComponent linkManagerFactory,
			PasswordResetMailSender passwordResetMailSender, ActivationMailSender activationMailSender) {
		this.userService = userService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.modelMapper = modelMapper;
		this.userConfig = userConfig;
		this.passwordResetMailSender = passwordResetMailSender;
		this.activationMailSender = activationMailSender;
		this.resetPasswordLinkManager = linkManagerFactory.createLinkManager(PASSWORD_RESET_API_LINK);
		this.activationLinkManager = linkManagerFactory.createLinkManager(ACTIVATION_API_LINK);
	}

	/**
	 * Registers a new user and sends the activation mail to the user's email
	 *
	 * @param user the user
	 * @return if the registration was successful
	 */
	@PostMapping("/register")
	public RegistrationResponse signUp(@Valid @DTO(RegisterApplicationUserDTO.class) ApplicationUser user) {
		if (userService.findByEmail(user.getEmail()).isPresent()) {
			return new RegistrationResponse(RegistrationStatus.USERNAME_TAKEN);
		}
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

		userService.save(user);

		LinkModel link = activationLinkManager.generateLink(userConfig.getLinkExpiration(), user);
		try {
			activationMailSender.sendMail(user, link);
		} catch (IOException e) {
			logger.error("An error occured", e);
			return new RegistrationResponse(RegistrationStatus.GENERAL_ERROR);
		}
		logger.info("Successfuly registered (but no activated) account \"{}\"", user.getEmail());

		return new RegistrationResponse(RegistrationStatus.SUCCESS);
	}

	/**
	 * Resends the activation email for the given user. If the user doesn't exist or
	 * is already actiavted then the method will silently fail
	 *
	 * @param user the user
	 */
	@PostMapping("/resendActivation")
	public void resendActivation(@RequestBody @DTO(EmailApplicationUserDto.class) ApplicationUser user) {
		// the name will be null if the application user wasn't mapped with the database
		if (user.getName() == null || user.isActivated()) {
			// silently fail if user doesn't exist or is already activated
			return;
		}

		LinkModel link = activationLinkManager.generateLink(userConfig.getLinkExpiration(), user);
		try {
			activationMailSender.sendMail(user, link);
		} catch (IOException e) {
			logger.error("An error occured", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Email Not Sent");
		}
		logger.info("Successfuly registered (but no activated) account \"{}\"", user.getEmail());
		return;
	}

	/**
	 * Activates the user which is associated with the given hash (also the hash is
	 * deactivated)
	 *
	 * @param hash the hash
	 */
	@PostMapping("/activate/{hash}")
	public void activate(@PathVariable("hash") String hash) {
		LinkModel linkModel = activationLinkManager.getLink(hash)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Hash Not Found"));
		activationLinkManager.deactivateLink(hash);
		ApplicationUser user = linkModel.getUser();
		user.setActivated(true);
		userService.save(user);
		logger.info("Activated user \"{}\"", user.getEmail());
	}

	/**
	 * Returns the currently logged in user
	 *
	 * @param principal the logged in use
	 * @return the user dto
	 */
	@GetMapping("/current")
	@Secured("ROLE_LOGGED_IN_USER")
	public ResponseApplicationUserDTO getCurrent(Principal principal) {
		String username = principal.getName();
		ApplicationUser user = userService.findByEmail(username).get();
		return modelMapper.map(user, ResponseApplicationUserDTO.class);
	}

	/**
	 * Initializes the password reset proccess for the given user. This means a mail
	 * with a special link is sent with which the user can reset his password
	 *
	 * @param principal the user email (as a user dto)
	 */
	@PostMapping("/initPasswordReset")
	public void initPasswordReset(@RequestBody EmailApplicationUserDto principal) {
		ApplicationUser user = userService.findByEmail(principal.getEmail())
				.orElse(null);
		// pretent to be successful, even if user doesn't exist
		// else attacker could check if user exists
		if (user == null) {
			logger.warn("User \"{}\" not found for password reset", principal.getEmail());
			return;
		}
		LinkModel link = resetPasswordLinkManager.generateLink(userConfig.getLinkExpiration(), user);
		try {
			passwordResetMailSender.sendMail(user, link);
		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Couldn't send mail", e);
		}
		logger.info("Initiated password reset for user \"{}\"", user.getEmail());
	}

	/**
	 * Resets the password of the user associated with the given hash and activates
	 * the account
	 *
	 * @param hash     the hash
	 * @param password the password
	 */
	@PostMapping("/resetPassword/{hash}")
	public void resetPassword(@PathVariable("hash") String hash,
			@RequestBody PasswordResetObj password) {
		LinkModel linkModel = resetPasswordLinkManager.getLink(hash)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Hash Not Found"));
		resetPasswordLinkManager.deactivateLink(hash);
		ApplicationUser user = linkModel.getUser();
		user.setPassword(bCryptPasswordEncoder.encode(password.getPassword()));
		user.setActivated(true);
		userService.save(user);
		logger.info("Reset password for user \"{}\"", user.getEmail());
	}
}
