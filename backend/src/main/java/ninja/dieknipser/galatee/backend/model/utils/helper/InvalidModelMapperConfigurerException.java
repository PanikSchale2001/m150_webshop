package ninja.dieknipser.galatee.backend.model.utils.helper;

/**
 * If a {@link ModelMapperConfiguration} is invalid
 *
 * @author sebi
 *
 */
public class InvalidModelMapperConfigurerException extends Exception {

	/**
	 * constructor
	 *
	 * @param message the message
	 * @param cause   the cause
	 */
	public InvalidModelMapperConfigurerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * constructor
	 *
	 * @param message the message
	 */
	public InvalidModelMapperConfigurerException(String message) {
		super(message);
	}

}
