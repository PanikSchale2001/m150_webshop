package ninja.dieknipser.galatee.backend.model.utils.helper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;

/**
 * A class which provides utils methods for the model mapper
 *
 * @author sebi
 *
 */
@Component
public class ModelMapperHelper {
	/**
	 * the logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * the scanning package
	 */
	public static final String SCANNING_PACKAGE = "ninja.dieknipser.galatee.backend";

	/**
	 * the model mapper instance
	 */
	private ModelMapper modelMapper;


	/**
	 * The {@link AutowireCapableBeanFactory} used for creating the
	 * {@link ModelMapperConfigurer}
	 */
	private AutowireCapableBeanFactory autowireCapableBeanFactory;

	/**
	 * Constructor
	 *
	 * @param modelMapper                the model mapper
	 * @param autowireCapableBeanFactory the factory used to create the
	 *                                   {@link ModelMapperConfigurer}
	 */
	public ModelMapperHelper(ModelMapper modelMapper, AutowireCapableBeanFactory autowireCapableBeanFactory) {
		this.modelMapper = modelMapper;
		this.autowireCapableBeanFactory = autowireCapableBeanFactory;
		findAndRunConfigurations();
	}

	/**
	 * Finds and runs instances of {@link ModelMapperConfigurer} which are annotated
	 * with {@link ModelMapperConfigurer} and calls the
	 * {@link ModelMapperConfigurer#configure(Configuration)}
	 *
	 * @see ModelMapperConfigurer#configure(Configuration)
	 */
	private void findAndRunConfigurations() {
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new AnnotationTypeFilter(ModelMapperConfiguration.class));
		for (BeanDefinition beanDef : provider.findCandidateComponents(SCANNING_PACKAGE)) {
			String className = beanDef.getBeanClassName();
			try {
				Class<?> clazz = Class.forName(className);
				if (ModelMapperConfigurer.class.isAssignableFrom(clazz)) {
					// ModelMapperConfigurer modelMapperConfigurer = (ModelMapperConfigurer)
					// applicationContext .getBean(clazz);
					ModelMapperConfigurer modelMapperConfigurer = (ModelMapperConfigurer) autowireCapableBeanFactory
							.autowire(clazz, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, true);
					logger.info("Calling the ModelMapperConfigurer \"{}\" to configure", className);
					modelMapperConfigurer.configure(modelMapper);
				} else {
					throw new InvalidModelMapperConfigurerException(
							"ModelMapperConfigurer \"" + className + "\" doesn't implement ModelMapperConfigurer");
				}
			} catch (Exception e) {
				logger.error("Cannot create \"{}\"", className, e);
			}
		}
	}

	/**
	 * Returns the model mapper
	 *
	 * @return the model mapper
	 */
	public ModelMapper getModelMapper() {
		return modelMapper;
	}

	/**
	 * Maps the given source to the destination class using the
	 * {@link ModelMapper#map(Object, Class)} method
	 *
	 * @param <D>    the destination type
	 * @param source the source object
	 * @param clazz  the destination class
	 * @return the mapped destination object
	 * @see ModelMapper#map(Object, Class)
	 */
	public <D> D map(Object source, Class<D> clazz) {
		return modelMapper.map(source, clazz);
	}

	/**
	 * Similar to {@link #map(Object, Class)} but maps over a list instead of just
	 * an object
	 *
	 * @param <D>        the destination type
	 * @param sourceList the source list with the objects to map
	 * @param clazz      the destination class
	 * @return the mapped destination object
	 * @see ModelMapper#map(Object, Class)
	 */
	public <D> List<D> mapAll(List<?> sourceList, Class<D> clazz) {
		return sourceList.stream().map(obj -> this.map(obj, clazz)).collect(Collectors.toList());
	}
}
