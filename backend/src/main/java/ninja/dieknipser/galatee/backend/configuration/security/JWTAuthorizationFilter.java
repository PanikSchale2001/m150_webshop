package ninja.dieknipser.galatee.backend.configuration.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String header = request.getHeader(SecurityConstants.HEADER_STRING);
		if (header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
			chain.doFilter(request, response);
			return;
		}

		UsernamePasswordAuthenticationToken token = getAuthentication(request);
		SecurityContextHolder.getContext().setAuthentication(token);
		chain.doFilter(request, response);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(SecurityConstants.HEADER_STRING);
		if (token != null) {
			DecodedJWT decodedToken = JWT.require(Algorithm.HMAC512(SecurityConstants.SECRET.getBytes()))
					.build()
					.verify(token.replace(SecurityConstants.TOKEN_PREFIX, ""));
			String user = decodedToken.getSubject();
			Claim claim = decodedToken.getClaim("roles");
			List<GrantedAuthority> authorities = new ArrayList<>();
			if (!claim.isNull()) {
				// if the claim isn't null then it tries to parse the claim as an array
				try {
					// then the roles are converted to SimpleGrantedAuthorities
					String[] authoritiesArray = claim.asArray(String.class);
					for (String authorityStr : authoritiesArray) {
						authorities.add(new SimpleGrantedAuthority(authorityStr));
					}
				} catch (JWTDecodeException e) {
					logger.warn("Couldn't decode \"roles\" claim", e);
				}
			}
			if (user != null) {
				return new UsernamePasswordAuthenticationToken(user, null, authorities);
			}
		}
		return null;
	}
}
