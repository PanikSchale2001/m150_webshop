package ninja.dieknipser.galatee.backend.mail;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ninja.dieknipser.galatee.backend.GalateeStartup;

/**
 * Represents a template with the subject and from who the mail should be sent
 *
 * @author sebi
 *
 */
public class MailTemplate {
	private String subject;
	private String template;
	private String from;


	/**
	 * @param subject
	 * @param template
	 * @param from
	 */
	public MailTemplate(String subject, String template, String from) {
		this.subject = subject;
		this.template = template;
		this.from = from;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Loads the template itself from a file. If the path starts with @ then it is
	 * loaded with {@link ClassLoader#getResourceAsStream(String)} and a template in
	 * the jar itself can be loaded
	 *
	 * @param filePath the file
	 * @param subject  the subject
	 * @param from     from who the mail is sent
	 * @return the mail template
	 * @throws IOException
	 */
	public static MailTemplate loadFrom(String filePath, String subject, String from) throws IOException {
		Logger logger = LoggerFactory.getLogger(MailTemplate.class);
		String template = null;
		if (filePath.startsWith("@")) {
			int atPos = filePath.indexOf('@');
			String resourcePath = filePath.substring(atPos + 1);
			// InputStream in = ClassLoader.getSystemResourceAsStream(resourcePath);
			InputStream in = GalateeStartup.class.getResourceAsStream(resourcePath);
			if (in == null) {
				logger.info("classpath: {}", System.getProperty("java.class.path"));
				throw new FileNotFoundException("Template at \"" + resourcePath + "\" not found by ClassLoader");
			}
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
				template = reader.lines().collect(Collectors.joining("\n"));
			} catch (UncheckedIOException e) {
				throw new IOException("An exception occurred during reading the template", e);
			}
		} else {
			template = new String(Files.readAllBytes(Paths.get(filePath)));
		}
		return new MailTemplate(subject, template, from);
	}
}
