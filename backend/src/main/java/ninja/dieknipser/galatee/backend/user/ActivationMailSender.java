package ninja.dieknipser.galatee.backend.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.UserConfig;
import ninja.dieknipser.galatee.backend.mail.MailController;
import ninja.dieknipser.galatee.backend.mail.MailTemplate;
import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;
import ninja.dieknipser.galatee.backend.trackedlinks.LinkModel;

/**
 * A class which sends password reset mails to users
 *
 * @author sebi
 *
 */
@Component
public class ActivationMailSender {
	/**
	 * the mail controller to send mails
	 */
	private MailController mailController;

	/**
	 * the user config
	 */
	private UserConfig userConfig;

	/**
	 * The password reset template
	 */
	private MailTemplate activationTemplate;

	/**
	 * Constructor
	 *
	 * @param mailController the mail controller
	 * @param userConfig     the user config
	 */
	public ActivationMailSender(MailController mailController, UserConfig userConfig) {
		this.mailController = mailController;
		this.userConfig = userConfig;
	}

	/**
	 * Tries to load the template from disk
	 *
	 * @throws IOException
	 */
	private void loadTemplateFromDisk() throws IOException {
		String templatePath = userConfig.getActivationTemplatePath();
		String from = userConfig.getMailSender();
		String subject = userConfig.getActivationSubject();
		activationTemplate = MailTemplate.loadFrom(templatePath, subject, from);
	}

	/**
	 * Returns the template. If this is the first time the template is accessed (or
	 * an error occurred in previous attempts) then the template is loaded from disk
	 * using the {@link UserConfig#getPasswordResetTemplatePath()}.
	 *
	 * @return the load template
	 * @throws IOException
	 */
	public MailTemplate getActivationTemplate() throws IOException {
		if (activationTemplate == null) {
			loadTemplateFromDisk();
		}
		return activationTemplate;
	}

	/**
	 * Sends the password reset mail to the given user
	 *
	 * @param user the user to send the mail to
	 * @param link the generated link
	 * @throws IOException if the template coulnt't be loaded
	 */
	public void sendMail(ApplicationUser user, LinkModel link) throws IOException {
		MailTemplate template = getActivationTemplate();
		Map<String, Object> variables = new HashMap<>();
		variables.put("link", link.getUrl());
		variables.put("user", user);
		variables.put("subject", template.getSubject());

		mailController.send(user.getName(), user.getEmail(), variables, template);
	}

}
