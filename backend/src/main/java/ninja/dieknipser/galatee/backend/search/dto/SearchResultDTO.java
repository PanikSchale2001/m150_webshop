package ninja.dieknipser.galatee.backend.search.dto;

import java.util.ArrayList;
import java.util.List;

import ninja.dieknipser.galatee.backend.model.shop.product.dto.ProductResponseDTO;

/**
 * The result dto of a search
 *
 * @author sebi
 *
 */
public class SearchResultDTO {
	/**
	 * the results
	 */
	private List<ProductResponseDTO> result = new ArrayList<>();

	/**
	 * the current page (index starts at zero)
	 */
	private int currentPage;
	/**
	 * the total number of pages (index starts at one)
	 */
	private int totalPages;

	/**
	 * Constructor
	 */
	public SearchResultDTO() {
	}

	/**
	 * @param result      the result lsit
	 * @param currentPage the current page
	 * @param totalPages  the number of pages
	 */
	public SearchResultDTO(List<ProductResponseDTO> result, int currentPage, int totalPages) {
		this.result = result;
		this.currentPage = currentPage;
		this.totalPages = totalPages;
	}

	/**
	 * @return the result
	 */
	public List<ProductResponseDTO> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(List<ProductResponseDTO> result) {
		this.result = result;
	}

	/**
	 * @return the currentPage
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage the currentPage to set
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * @return the totalPages
	 */
	public int getTotalPages() {
		return totalPages;
	}

	/**
	 * @param totalPages the totalPages to set
	 */
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

}
