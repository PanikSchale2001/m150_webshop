package ninja.dieknipser.galatee.backend.model.shop.product.dto;

import java.util.ArrayList;
import java.util.List;

public class ProductResponseDTO {
	private long id;

	private String name;

	private String description;

	private double price;
	private double weight;

	private List<String> images = new ArrayList<>();

	/**
	 * Constructor
	 */
	public ProductResponseDTO() {
	}

	/**
	 * Constructor
	 *
	 * @param id          the id
	 * @param name        the name
	 * @param description the description
	 * @param price       the price
	 * @param weight      the weight
	 * @param images      the images
	 */
	public ProductResponseDTO(long id, String name, String description, double price, double weight,
			List<String> images) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.weight = weight;
		this.images = images;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}

	/**
	 * @return the images
	 */
	public List<String> getImages() {
		return images;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(List<String> images) {
		this.images = images;
	}

}
