package ninja.dieknipser.galatee.backend.trackedlinks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ninja.dieknipser.galatee.backend.configuration.applicationconfig.GeneralConfig;
import ninja.dieknipser.galatee.backend.model.user.ApplicationUser;

/**
 * Tests the {@link LinkManager}
 *
 * @author sebi
 *
 */
public class TrackedLinkManagerTest {
	/**
	 * logger
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * the base url in the tests
	 */
	private static final String TEST_BASE_URL = "example.com";
	/**
	 * the api url in the tests
	 */
	private static final String TEST_API_URL = "/test/";
	/**
	 * the tracked link manager used in tests
	 */
	private LinkManager manager;

	/**
	 * Constructor
	 */
	public TrackedLinkManagerTest() {
	}

	/**
	 * Sets up the tracked link manager
	 */
	@BeforeEach
	public void setup() {
		GeneralConfig config = new GeneralConfig();
		config.setHashLength(20);
		manager = new LinkManager<>(TEST_BASE_URL, TEST_API_URL, new LinkFactory.DefaultLinkFactory(),
				new HashGenerator(config));
		manager.setLinkTerminationSleep(500); // set link termination time to 0.5 seconds
		manager.start();
	}

	/**
	 * Tears the manager down
	 */
	@AfterEach
	public void tearDown() {
		manager.stop();
	}

	/**
	 * tests the creation of links
	 */
	@Test
	public void testLinkCreation() {
		ApplicationUser user = new ApplicationUser();
		long expirationTime = 60 * 1000; // 1 minute
		long start = System.currentTimeMillis();
		LinkModel link = manager.generateLink(expirationTime, user);
		assertEquals(user, link.getUser());
		assertEquals(TEST_BASE_URL, link.getBaseUrl());
		assertEquals(TEST_API_URL, link.getApiUrl());
		assertEquals(TEST_BASE_URL + TEST_API_URL + link.getHash(), link.getUrl());
		assertNotNull(link.getHash());
		LocalDateTime expirationDate = link.getExpiresAt();
		long expirationTimestamp = expirationDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
		assertTrue(expirationTimestamp > (start + expirationTime) - 200, "The expiration time is too big");
		assertTrue(expirationTimestamp < (start + expirationTime) + 200, "The expiration time is too small");
	}

	/**
	 * Tests if links expire correctly
	 */
	@Test
	public void testExpiration() {
		ApplicationUser user = new ApplicationUser();

		// creates links based on these expiration times
		long[] expirationTimes = new long[] { 1000, 3 * 1000, 3 * 1000, 5 * 1000 };
		LinkModel[] links = new LinkModel[expirationTimes.length];
		long maxTime = 0; // tracks the max expiration time set
		for (int i = 0; i < expirationTimes.length; i++) {
			links[i] = manager.generateLink(expirationTimes[i], user);
			if(expirationTimes[i] > maxTime) {
				maxTime = expirationTimes[i];
			}
		}
		logger.info("Max time is: " + maxTime);
		long start = System.currentTimeMillis();
		long current = System.currentTimeMillis();
		// a 1 second buffer/delay time
		while (start + maxTime + 2000 > current) {
			current = System.currentTimeMillis();
			for (int i = 0; i < expirationTimes.length; i++) {
				// if the start + expirationTime of current link + 0.5 second buffer is before
				// the current time, then the link should be expired
				if (start + expirationTimes[i] + 500 < current) {
					if (!links[i].isExpired()) {
						// logger.info("time mak");
					}

					assertTrue(links[i].isExpired(), "link " + i + " with the expiration time " + expirationTimes[i]
							+ " should be expired, but isn't");

					// if the start time + the expirationTime of the link minus a buffer of 0.5
					// second is greater than now,
					// then the link shouldn't be expired
				} else if (start + expirationTimes[i] - 500 > current) {
					assertFalse(links[i].isExpired(), "link " + i + " with the expiration time " + expirationTimes[i]
							+ " is expired, but shouldn't");
				}
			}
		}
		long countValidLinks = Arrays.stream(links).filter(l -> !l.isExpired()).count();
		logger.info("valid links: {}", countValidLinks);
		assertEquals(0, countValidLinks,
				"There shouldn't be any valid links left, but \"" + countValidLinks + "\" links are still valid");
	}
}
