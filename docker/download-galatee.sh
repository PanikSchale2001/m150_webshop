#!/bin/bash
MVN_CLI_OPTS="$1"
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pushd "$dir"
echo "copy pom.xml to $dir"
cp "../backend/pom.xml" .

version=$(mvn $MVN_CLI_OPTS org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate -Dexpression=project.version -q -DforceStdout --batch-mode -U -e -Dsurefire.useFile=false)
echo "Maven version is: \"$version\""

echo "remove pom.xml"
rm pom.xml

echo "Download jar \"ninja.dieknipser.galatee:galatee-backend:$version\""
mvn $MVN_CLI_OPTS dependency:get -Dartifact="ninja.dieknipser.galatee:galatee-backend:$version" -DremoteRepositories=https://gitlab.com/api/v4/projects/22173002/packages/maven
mvn $MVN_CLI_OPTS dependency:copy -Dartifact="ninja.dieknipser.galatee:galatee-backend:$version" -DremoteRepositories=https://gitlab.com/api/v4/projects/22173002/packages/maven -DoutputDirectory=.

echo "Rename maven dependency jar to galatee.jar"
find ./ -maxdepth 1 -name "galatee-backend*" -print -exec mv "{}" galatee.jar \;

