import Vue from 'vue'
import Router from 'vue-router'
import LoginPageVue from '@/pages/login/LoginPage.vue'
import RegisterPageVue from '@/pages/login/RegisterPage.vue'
import AccountActivationVue from '@/pages/login/AccountActivation.vue'
import ForgotPasswordPageVue from '@/pages/login/ForgotPasswordPage.vue'
import PasswordResetPageVue from '@/pages/login/PasswordResetPage.vue'
import ProductListPageVue from '@/pages/product/ProductListPage.vue'
import ProductPageVue from '@/pages/product/ProductPage.vue'
import CheckoutPageVue from '@/pages/cart/CheckoutPage.vue'
import AddressPayPageVue from '@/pages/cart/AddressPayPage.vue'
import SearchPageVue from '@/pages/search/SearchPage.vue'
import ThankYouPageVue from '@/pages/cart/ThankYouPage.vue'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'Products',
      component: ProductListPageVue
    },
    {
      path: '/product/:id',
      name: 'Product',
      component: ProductPageVue
    },
    {
      path: '/search',
      name: "Search",
      component: SearchPageVue
    },
    {
      path: '/checkout',
      name: 'Checkout',
      component: CheckoutPageVue
    },
    {
      path: '/checkout/pay',
      name: 'Address and Pay',
      component: AddressPayPageVue
    },
    {
      path: '/checkout/thankyou',
      name: 'Thankyou',
      component: ThankYouPageVue
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginPageVue
    },
    {
      path: '/register',
      name: 'Register',
      component: RegisterPageVue
    },
    {
      path: '/register/activation/:hash',
      name: "Activation",
      component: AccountActivationVue
    },
    {
      path: '/register/resetPassword',
      name: "Init Password Reset",
      component: ForgotPasswordPageVue
    },
    {
      path: '/register/resetPassword/:hash',
      name: "ResetPassword",
      component: PasswordResetPageVue
    }
  ]
})
