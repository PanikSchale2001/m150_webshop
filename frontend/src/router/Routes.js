export default new class Routes {
    constructor() {
    }

    _init(router) {
        this.router = router;
    }

    register(id) {
        this.router.push(`/register`)
    }
}