import { simpleFetch, simpleFetchAuthed } from "@/model/rest/RestUtils";

const LOGIN_URL = "/login";
const TOKEN_HEADER = "Authorization";

export const SET_TOKEN = "setToken";
export const SET_USER = "setUser";

export default {
    namespaced: true,
    state: {
        user: null,
        token: null
    },
    getters: {
        isLoggedIn(state) {
            return state.token != null && state.user != null;
        }
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
        },

        setToken(state, token) {
            state.token = token;
            sessionStorage.setItem("user.token", token);
        }
    },
    actions: {
        async register(context, { name, email, password }) {
            const response = await simpleFetch("/user/register", "POST", { name, email, password })
                .notOk(response => `Couldn't register because of ${response.statusText} (${response.status})`)

            return response.json();
        },
        async login(context, { email, password }) {
            const response = await fetch(LOGIN_URL, {
                method: "POST",
                mode: "cors",
                cache: "no-cache",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json"
                },
                redirect: "follow",
                referrerPolicy: "no-referrer",
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })

            if (!response.ok) {
                throw new RestError(url, response, `Couldn't login because of ${response.statusText} (${response.status})`)
            };

            const token = response.headers.get(TOKEN_HEADER);
            if (token == null) {
                throw new RestError(url, response, `Recieved token is null`)
            }
            await context.commit(SET_TOKEN, token);
            await context.dispatch("init");
            // attach current cart to user
            context.dispatch("cart/attachToUser", null, { root: true })
        },
        logout(context) {
            context.commit(SET_TOKEN, null);
            context.commit(SET_USER, null);
            context.dispatch("cart/deattachFromUser", null, { root: true })
        },
        async init(context) {
            let token = context.state.token;
            if (token == null) {
                const sessionToken = sessionStorage.getItem("user.token");
                if (sessionToken != null && sessionToken != "null") {
                    token = sessionToken;
                } else {
                    // return because we have no token and are not logged in
                    return;
                }
            }
            const response = await simpleFetch("/user/current", "GET", null, { token: token })
            context.commit(SET_USER, await response.json());

            // if the token is from the session token
            if (context.state.token != token) {
                context.commit(SET_TOKEN, token)
            }

        },
    }
}