import { simpleFetch, simpleFetchAuthed } from "@/model/rest/RestUtils";
export default {
    namespaced: true,
    state: {
        /**
         * the products
         */
        products: [],
        /**
         * the search term
         */
        searchTerm: "",
        /**
         * the latest fetched page
         */
        page: -1,
        /**
         * if currently a product is loading
         */
        loading: false,
        /**
         * If the last page was loaded
         */
        reachedEnd: false
    },
    getters: {
    },
    mutations: {
        /**
         * Adds products to the store and updates the page
         * @param {*} state the state
         * @param {Object} object the keys are products and currentPage for the products to add and the latest page
         */
        addSearchResult(state, { products = [], currentPage }) {
            state.page = currentPage
            state.products = products
        },

        /**
         * sets the search term and resets the page and products
         * @param {*} state the state
         * @param {String} searchTerm the search term
         */
        setSearchTerm(state, searchTerm) {
            state.searchTerm = searchTerm
            state.page = -1
            state.reachedEnd = false
            state.products = []
        },

        /**
         * Sets the product loading state to true or false
         * @param {*} state the state
         * @param {Boolean} loading the loading state
         */
        setLoading(state, loading) {
            state.loading = loading;
        },

        /**
         * Sets if the end was reached
         * @param {*} state the state
         * @param {Boolean} reachedEnd if the end was reached
         */
        setReachedEnd(state, reachedEnd) {
            state.reachedEnd = reachedEnd;
        }
    },
    actions: {
        /**
         * Fetches the given page
         * @param {*} context the context
         * @param {Number} page the page number to fetch
         */
        async fetchNextPage(context) {
            if (context.state.reachedEnd) {
                console.warn("Reached end")
                return;
            }
            // prevent from fetching the next page, if a page is already loading
            if (context.state.loading) {
                console.warn("Trying to fetch the next page, while already loading a page")
                return;
            }
            context.commit("setLoading", true)
            try {
                const searchTerm = context.state.searchTerm
                const nextPage = context.state.page + 1

                const productResponse = await simpleFetch(`/search/${searchTerm}/${nextPage}`).notOk("Couldn't fetch the next page").json()

                context.commit("addSearchResult", { products: productResponse.result, currentPage: nextPage })
                context.commit("setReachedEnd", productResponse.currentPage + 1 == productResponse.totalPages)
            } finally {
                context.commit("setLoading", false)
            }

        },
        async search(context, searchTerm) {
            if (context.state.loading || context.state.searchTerm == searchTerm) return;
            await context.commit("setSearchTerm", searchTerm)
            await context.dispatch("fetchNextPage")
        }

    }
}