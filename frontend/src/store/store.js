import cartStore from '@/store/cartStore'
import loginStore from '@/store/loginStore'
import productStore from '@/store/productStore'
import searchStore from '@/store/searchStore'

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        user: loginStore,
        product: productStore,
        cart: cartStore,
        search: searchStore
    }
})

async function initStore() {
    await store.dispatch("user/init");
    await store.dispatch("product/init");
    await store.dispatch("cart/init");
}
initStore()

export default store;