import { simpleFetch, simpleFetchAuthed } from "@/model/rest/RestUtils";

const LOCAL_STORAGE_CART_ID = "cart.id"
function getCartIdFromLocalStorage() {
    return localStorage.getItem(LOCAL_STORAGE_CART_ID)
}

function setCartIdToLocalStorage(id) {
    localStorage.setItem(LOCAL_STORAGE_CART_ID, id)
}

export default {
    namespaced: true,
    state: {
        cart: {
            id: "",
            productEntires: [],
            totalAmount: 0,
            totalAmountWithTax: 0
        },
        useUserCart: false,
        loading: false,
        sidebarOpen: false,
    },
    /**
     * returns the id of the current cart
     */
    getters: {
        cartId(state) {
            return state.cart.id;
        }
    },
    mutations: {
        setCart(state, cart) {
            state.cart = cart;
            setCartIdToLocalStorage(cart.id)
        },
        setLoading(state, loading) {
            state.loading = loading;
        },
        setSidebarOpen(state, sidebarOpen) {
            state.sidebarOpen = sidebarOpen;
        }
    },
    actions: {
        async init(context) {
            context.commit("setLoading", true)
            try {
                if (context.rootGetters["user/isLoggedIn"]) {
                    const cart = await simpleFetchAuthed(context.rootState, "/cart/userCart").notOk("Couln't get user's cart").json();
                    context.commit("setCart", cart);
                } else {
                    const cartId = getCartIdFromLocalStorage()
                    const cart = await simpleFetch(`/cart/${cartId}`).notOk(`Couldn't get a cart (cart id: "${cartId}"`).json();
                    context.commit("setCart", cart);
                }
            } catch (e) {
                // write error 
                console.error("couldn't init", e);
                throw e;
            } finally {
                context.commit("setLoading", false);
            }
        },
        async addProduct(context, { product, counterToAdd }) {
            context.commit("setLoading", true)
            try {
                const newCart = await simpleFetch(`/cart/${context.getters.cartId}/add/${product.id}/${counterToAdd}`, "POST")
                    .notOk(`Couldn't add product ${product.name} (id: ${product.id}) to cart`)
                    .json()
                context.commit("setCart", newCart);
                return newCart;
            } finally {
                context.commit("setLoading", false)
            }
        },
        async removeProductEntry(context, product) {
            context.commit("setLoading", true)
            try {
                const newCart = await simpleFetch(`/cart/${context.getters.cartId}/remove/${product.id}`, "DELETE")
                    .notOk(`Couldn't remove product ${product.name} (id: ${product.id}) from cart`)
                    .json()
                context.commit("setCart", newCart);
                return newCart;
            } finally {
                context.commit("setLoading", false)
            }
        },
        async attachToUser(context) {
            const newCart = await simpleFetchAuthed(context.rootState, `/cart/${context.getters.cartId}/attachToUser`, "POST").notOk("Couldn't merge cart with user cart").json()
            context.commit("setCart", newCart)
            return newCart
        },
        async deattachFromUser(context) {
            const newCart = await simpleFetchAuthed(context.rootState, `/cart/${context.getters.cartId}/copyCart`, "POST").notOk("Couldn't deattach/copy cart").json()
            context.commit("setCart", newCart)
            return newCart
        },
        async clearCart(context) {
            let newCart = null;
            if (context.rootGetters["user/isLoggedIn"]) {
                newCart = await simpleFetchAuthed(context.rootState, `/cart/${context.getters.cartId}/clear`, "POST").notOk("Couldn't deattach/copy cart").json()
            } else {
                newCart = await simpleFetch(`/cart/${context.getters.cartId}/clear`, "POST").notOk("Couldn't deattach/copy cart").json()
            }
            await context.commit("setCart", newCart)
            return newCart;
        }
    }
}