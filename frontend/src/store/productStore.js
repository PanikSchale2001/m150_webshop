import { simpleFetch, simpleFetchAuthed } from "@/model/rest/RestUtils";
export default {
    namespaced: true,
    state: {
        /**
         * the products
         */
        products: [],
        /**
         * the latest fetched page
         */
        page: -1,
        /**
         * if currently a product is loading
         */
        loading: false,
        /**
         * If the last page was loaded
         */
        reachedEnd: false
    },
    getters: {
    },
    mutations: {
        /**
         * Adds products to the store and updates the page
         * @param {*} state the state
         * @param {Object} object the keys are products and currentPage for the products to add and the latest page
         */
        addProducts(state, { products = [], currentPage }) {
            state.page = currentPage;
            // concats the state.products with the new products
            state.products.push.apply(state.products, products);
        },
        /**
         * Sets the product loading state to true or false
         * @param {*} state the state
         * @param {Boolean} loading the loading state
         */
        setLoading(state, loading) {
            state.loading = loading;
        },

        /**
         * Sets if the end was reached
         * @param {*} state the state
         * @param {Boolean} reachedEnd if the end was reached
         */
        setReachedEnd(state, reachedEnd) {
            state.reachedEnd = reachedEnd;
        }
    },
    actions: {
        /**
         * initializes this store
         * @param {*} context the context
         */
        async init(context) {
            context.dispatch("fetchNextPage");
        },
        /**
         * Fetches the given page
         * @param {*} context the context
         * @param {Number} page the page number to fetch
         */
        async fetchNextPage(context) {
            if (context.state.reachedEnd) {
                console.warn("Reached end")
                return;
            }
            // prevent from fetching the next page, if a page is already loading
            if (context.state.loading) {
                console.warn("Trying to fetch the next page, while already loading a page")
                return;
            }
            context.commit("setLoading", true)
            const nextPage = context.state.page + 1;

            const productResponse = await simpleFetch(`/products/${nextPage}`).notOk("Couldn't fetch the next page").json()

            context.commit("addProducts", { products: productResponse.productList, currentPage: nextPage })
            context.commit("setReachedEnd", productResponse.pagesLeft == 0)
            context.commit("setLoading", false)

        },
        /**
         * Fetches the product from the server with the given id
         * @param {*} context the context
         * @param {number} id the id of the product
         */
        async fetchProduct(context, id) {
            return await simpleFetch(`/product/${id}`).notOk("Couldn't fetch Product (id: " + id + ")").json();
        }

    }
}