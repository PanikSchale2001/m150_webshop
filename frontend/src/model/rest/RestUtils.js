export const BASE_URL = "/api";

export function simpleFetchAuthed(state, url, method = "GET", body = null, options = {}) {
    const token = state.user.token;
    options.token = token;
    return simpleFetch(url, method, body, options);
}

export function simpleFetch(url, method = "GET", body = null, { contentType = "application/json", token = null, stringifyBody = true } = {}) {
    const options = {
        method,
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {},
        redirect: "follow",
        referrerPolicy: "no-referrer",
    };
    if (body != null) {
        if (typeof body == "string" && !stringifyBody) {
            options.body = body;
        } else {
            options.body = JSON.stringify(body);
        }
        if (contentType != null) {
            options.headers["Content-Type"] = contentType;
        }
    }
    if (token != null) {
        options.headers["Authorization"] = "Bearer " + token;
    }
    let promise = fetch(BASE_URL + url, options).then(response => {
        if (response.status == 403) {
            window.location.hash = "/login";
            window.location.reload();
            throw new LoginError(url, response, "Not logged in");
        }
        return response;
    })
    return _enhancePromise(promise)
}

/**
 * Enhances the promise with some network related functions
 * @param {Promise} promise the promise to enhance
 */
function _enhancePromise(promise) {
    // prevent double wrapping promises
    if (promise.enhanced) return promise;
    promise.enhanced = true;

    // not overriden promise functions
    const _then = promise.then;
    const _catch = promise.catch;
    const _finally = promise.finally
    /**
     * overriding the then function makes sure that the enhanced promise stays enhanced
     */
    promise.then = function () {
        const internalPromise = _then.apply(this, arguments);
        return _enhancePromise(internalPromise);
    }
    /**
     * overriding the catch function makes sure that the enhanced promise stays enhanced
     */
    promise.catch = function () {
        const internalPromise = _catch.apply(this, arguments);
        return _enhancePromise(internalPromise);
    }
    /**
     * overriding the catch function makes sure that the enhanced promise stays enhanced
     */
    promise.finally = function () {
        const internalPromise = _finally.apply(this, arguments);
        return _enhancePromise(internalPromise);
    }

    /**
     * Checks if the given code matches the status code of the response. If it does
     * the given function is called.
     * If the function returns a string, this string is used as the error message of
     * a rejected promise. (if no string is returned, then no rejected promise is returned).
     * 
     * If the second argument isn't a function then this value is directly used as the message of 
     * the rejected promise (without calling a function first)
     * 
     * @param {number|string} code the code which has to be matched
     * @param {function|string} fun the function to call if the code matches  or the error message
     */
    promise.statusCode = function (code, fun) {
        const internalPromise = this.then(response => {
            // checks if the status code matches the code or if the code starts the given code
            if (response.status == code || (response.status + "").indexOf(code) == 0) {
                if (typeof fun == "function") {
                    let errorMsg = fun(response);
                    if (typeof errorMsg == "string") {
                        return Promise.reject(errorMsg);
                    }
                } else {
                    return Promise.reject(fun);
                }
            }
            return response;
        })
        return internalPromise;
    }
    /**
     * If the response is a 2xx response then the function is called
     * @param {function} fun the function
     */
    promise.ok = function (fun) {
        const internalPromise = this.then(response => {
            if (response.ok) {
                fun(response);
            }
            return response;
        })
        return internalPromise;
    }

    /**
     * Calls then function when the response isn't ok. 
     * If the function return a string then the string is
     * used as a message in a rejected promise.(if no string is returned, then no rejected promise is returned).
     * 
     * If the given argument isn't a function, then it is used as the error message 
     * of the rejected promise (without calling a function)
     * 
     * @param {function|string} fun the function or error message
     */
    promise.notOk = function (fun) {
        return this.then(response => {
            if (!response.ok) {
                if (typeof fun == "function") {
                    let errorMsg = fun(response);
                    if (typeof errorMsg == "string") {
                        return Promise.reject(errorMsg);
                    }
                } else if (fun == null) {
                    return Promise.reject("A general error occured");
                } else {
                    return Promise.reject(fun);
                }
            }
            return response;
        })
    }

    /**
     * If the argument is a function, the function is called with the object recieved by then and if the function 
     * returns a string value it is used as the message of a rejected promise. However is the return value something else
     * then nothing happends
     * 
     * If the argument is an other non null value it is directly used as the message of a rejected promise.
     * @param {function|string} fun the function which is called or a string which is used as the error message of the rejected promise
     */
    promise.error = function (fun) {
        return this.then(response => {
            if (typeof fun == "function") {
                let errorMsg = fun(response);
                if (typeof errorMsg == "string") {
                    return Promise.reject(errorMsg);
                }
            } else if (fun != null) {
                return Promise.reject(fun);
            } else {
                return promise;
            }
        })
    }

    /**
     * Helper function to get the json in one line
     */
    promise.json = function () {
        return this.then(response => response.json())
    }

    return promise;
}

export class RestError extends Error {
    constructor(url, response, msg) {
        super(msg)
        this.url = url;
        this.response = response;
    }
}

export class LoginError extends RestError {
    constructor(url, response, msg) {
        super(url, response, msg)
    }
}