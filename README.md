---
geometry: margin=1in
numbersections: true
toc: true
toc-title: Inhaltsverzeichnis
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \setlength\headheight{20pt}
    \lhead{M150 - WebShop Projekt}
    \rhead{Jeriel Frei \& Sebastian Zumbrunn}
---

Ziel
====

Einen Webshop zu entwickeln um Tee zu verkaufen. Der Fokus soll auf
dem Login System und Warenkorb liegen und daher auch Produktanzeige.

Eine Demoseite kann man auf [https://galatee.dieknipser.ninja](https://galatee.dieknipser.ninja) 
angesehen werden. Login, Registration, Produkt-Suche, Warenkorb bis zum Chekout sollten alles
funktionieren.

Schwerpunkt
===========
Das Ziel ist vorallem das Loginsystem mit samt Passwordzurücksetzungs-Mail und Activation-Mail und
Produkte anzuzeigen zusammen mit dem Warenkorb. In Klammern im Titel ist jeweil beschrieben, ob wir
es umgesetzt haben oder nicht.

Zeitschätzung
=============
Zeitschätzungen werden mit der Dreipunkteschätzung getan und sind in den Kapiteln
der jeweiligen System untergebracht.

Einzelteile
===========

Hier unten werden die einzelnen Teile/Module der Projektarbeit
aufgelistet.

Login-System
------------

Das Login System kümmert sich um das einloggen, wie auch Registrieren
von neuen Nutzer. Dies ist einer der Systeme, welche wir umsetzen möchten.

### Zeitschätzung
Folgendes ist zu implementieren:
(optimistisch, realistisch, pesimistisch)
* Einloggen
	* JWT Token handling in Spring Security (3h, 4h, 6h)
	* Annonymer Nutzer (1h, 2h, 3h)
* Registrieren (3h, 5h, 6h)
	* Aktivierungsmail versenden (welches nach einer Zeit abläuft)
* Passwort zurück setzen (1h, 2h, 4h)
	* Mail versenden mit Link, welcher abläuft
* Datenanbindung (oben bereits eingerechnet)
* DTOs für REST Apis (1h, 2h, 2h)

Optimistisch: 3h + 1h + 3h + 1h + 1h = 9h
Realistisch: 4h + 2h + 5h + 2h + 2h = 15h
Pessimistisch: 6h + 3h + 6h + 4h + 2h = 21h
Schätzwert: 15h

Gebraucht: 4h + 3h + 4h + 6h + 6h = 23

### Registrieren (umgesetzt)

Beim Registrieren soll der Nutzer seinen Namen, E-Mail-Adresse und
Passwort angeben. Dabei soll das Password auf dem Server gehasht werden
(und die Seite über HTTPS übertragen werden).

### Login (umgesetzt)

Der Login ist wie erwartet. Der Nutzer gibt sein Nutzername und Passwort
ein und auf dem Server wird geprüft, ob das gehashte Passwort mit dem in
der DB übereinstimmt und der Nutzername existiert. Wenn dies der Fall
ist, wird ein JWT Token zurückgegeben, mit welchem sich der Benutzer
oder Client in der Zukunft anmeldet.

### Passwort Zurücksetzen (umgesetzt)

Auf der Loginseite gibt es ein Knopf, welchen der Password
Zurücksetzt-Prozess beginnt. Dabei wird ein Mail an die angegebene
Mailadresse abgesendet, mit einem Random Link, welcher nur eine
limitierte Zeit gültig ist. Wenn dieser angeklickt wird, kann der Nutzer
sein Passwort zurücksetzen. Dabei ist es wichtig zu erwähnen, dass der
Link in der DB erfasst ist, damit kontrolliert werden kann, ob der Link
noch gültig ist.

### Risiken

Wenn das Login System nicht sauber funktioniert könnte jemand zugriff auf Funktionen
erhalten, auf welche er nicht zugriff haben sollte, wie z.B. neue Produkte erfassen, 
Preise verändern oder Nutzerverwaltung. Degen helfen vorallem Manuelle Tests. Das
Problem bei Unit Tests ist, dass man die Konfiguration, wie, welche URLs ohne 
Authentication erreichbar sind, schlecht Unit Testen kannn.

Shop
----

### Produkte Anzeigen (umgesetzt)
### Zeitschätzung
Folgendes ist zu implementieren:
Die Produkte müssen in der Datenbank gespeichert werden und müssen dem User angezeigt werden. (8h,9h,10h)

### Produkte Filtern & Suche (zum Teil umgesetzt)
Ein Shop sollte eine Liste aller Produkte haben auf welcher man suchen und nach geeigneten Kategorien filtern kann.
Davon ist nur die Suche umgesetzt.
Bei dieser gibt es ein /search Endpoint, welchem vom Frontend jedes mal angefragt wird, wenn der User kein Input
für 700ms eingegeben hat (dies ist da, um den Server zu entlasten). Die Zurückgegebene Liste wird angezeigt.

Das Filtern ist hingegen nicht umgesetzt. Wenn wir dies tun würden, würden wir den Produkten Kategorien zuweisen und
pro Kategorie Felder mit Eigenschaften (wie Gewicht, Geschmack, Typ, ...) definieren. Auf dem Produkt würde man dann die
Felder mit Werten "befüllen". Somit hat man ein relativ flexibles System, welches sich durchsuchen lässt. 

Die Suche müsste man mit @Query Annoation oder Springs Criterias API implementieren, da wir hier weit weg von
JpaRepositorys Standart Funktionen sind. Im Frontend bräuchte es natürlich noch die entsprechende Page damit man
den Fiter auswählen und anzeigen kann.

### Warenkorb (umgesetzt)
Wenn ein Kunde in einem Shop ein Produkt kaufen will sollte es zuerst in einen Warenkorb hinterlegt werden. 
In einem Warenkorb sollte man alle Produkte sehen und diese verwalten zudem sollte man den gesamt Preis sehen.

Die ID des Warenkorb wird in der Session gespeichert (localStorage). Wenn noch keine ID gespeichert ist oder
der Warenkorb nicht mehr existiert, dann wird ein neuer Warenkorb erstellt. Die generierte ID ist dabei
mit SecureRandom generiert und sollte kryptographisch sicher sein.

Wie auch bei Links, sollten Warenkörbe, welche nicht in der DB sind, mit der Zeit abgeräumt werden (nach einigen Tagen, in welchen 
der Warenkorb nicht benutzt wurde), da jede offene Session immer einen Warenkorb hat und so schnell
viel RAM benutzt wird. Leere Warenkörbe sollten schon schneller gelöscht werden, auch solche in der DB, da diese
nicht benötigt werden.

#### Benutzer Warenkorb (umgesetzt)
Jeder Benutzer hat ein Warenkorb, welcher persistent in der DB gespeichert wird.
Dies erlaubt es, dass er Dinge in seinen Warenkorb hinzufügt und auf einem anderem
Gerät ebenfalls hat. Wenn man sich einloggt und bereits Dinge im Warenkorb hat, 
dann werden diese Items zum Nutzerwarenkorb hinzugefügt. Wenn man sich aussloggt,
dann wird der aktuelle Warenkorb kopiert, damit man nicht weiterhin den Nutzerwarenkorb
bearbeitet, da dafür nur die ID des Warenkorb benützt wird.

### Mehrwertssteuer (umgesetzt)
Die Mehrwertssteuer soll pro Produkt anpassbar sein. Im Frontend wird jeweils der Preis
ohne und mit Mehrwertssteuer und die Mehrwertssteuer selbst angezeigt. 
Bei den Produkten selbst wird der Preis *ohne* Mehrwertssteuer gezeigt, da dieser
billiger ist.

Zeitschätzung: 
- Optimistisch: 1.5h
- Realistisch: 2h
- Pesimistisch: 4h
Schätzwert: 2.25h
Wirklicher Aufwand: 2h


### Bezahlen
In einem Shop muss man online bezahlen können. Dafür gibt es mehrere Möglichkeiten wie auf Rechnung oder per Kreditkarte. 
Dies würden wir allerdings nicht selbst implementieren, da die nötigen Zertifizierungen viel zu viel Geld kosten und zu 
viel Aufwand sind. Anstatt würden wir ein (oder mehrere) Provider, wie Paypal, Postfinance oder Stripe benützen. Für
die Rechnung würd man über eine API der Bank gehen, bei welcher wir das Konto hätten.

Sehr wichtig ist, dass nach dem der Benutzer gezahlt hat und wir vom Provider die Erfolgsmeldung erhalten, der Ablauf ohne
Fehler läuft - *immer* - da keiner mag Geld zu bezahlen und nichts dafür zu bekommen. Wenn hier Fehler entstehen, müssen diese
Umbeding in einer Statistik in zB. Elastic Search auftauchen, damit diese sofort behoben werden können.

Shop Management
---------------
Unser Shop hat kein Admin Bereich. Dies wäre natürlich ein grosser Punkt, welcher
für ein "richter" Online Shop umgesetzt werden müsste, speziell weil "normale" 
Mitarbeiter (ohne SQL Kentnisse) damit arbeiten müssten. 

### Produkte Verwalten (nicht umgesetzt)
Als Admin muss man die Produkte verwalten können, dazu gehört Produkte erfassen, editieren und löschen zu können.

Dafür könnte man das bereits existierende GUI verwenden und eine "Admin" Rolle definieren. Wenn ein Nutzer
zur dieser Rolle/Gruppe gehhört, dann Textfelder anzeigen, auf der Produktseite und noch ein "Add"- und 
"Delete"-Button für die Produkte. Wichtig wäre auch noch Statistiken und Lagerbestände, da Tee ein physikalisches
Produkt wäre. Dies könnte man auf der Produkt-Page selbst hinzufügen oder eine seperate Seite dafür erstellen.

### Nutzerverwaltung (nicht umgesetzt)
Wenn es ein "richtiger" Online Shop wäre, welcher gebraucht würde, dann wäre eine
Nutzerverwaltung unverzichbar. Zum einen, weil man Rechte für Produkte vergeben müsste, 
aber auch weil man Nutzer löschen oder sperren möchte können oder andere
Verwaltungs aufgaben.

Folgende Features wären vemutlich für Version 1.0 nötig (optimistisch / realitstisch / pesimistisch):
- Nutzer löschen (und Resourcen, wie Warenkorb aufräumen/löschen) (3h / 5h / 10h)
- Admin Rechte verteilen/entziehen (10h / 13h / 15h)

Schätzwärt: 49.7h
Wirklicher Aufwand: - (da nicht umgesetzt)

Errorhandling (wenig umgesetzt)
=============
Es gibt zwei Teile im Errorhandling: 
Systeme, wie Loggers im Backend oder auch sauberes Verhalten im Fehlerfall im Frontend
ist implementiert. 
Im Frontend ist eines der grössten Fehlerpotenzial die Netzwerkaufrufe an die 
Rest-Endpoints, da hier einiges Schieflaufen kann (Server Fehler, plötzlich Offline, ...)
Diese Fehler sollten gut abgefangen sein durch Try-Catches und durch die RestUtils.js
Klasse (im JS Teil), welche Fehlerhandling implementiert.

Was hingegen fehlt, ist, dass die JS Fehler (und Warnings) repotiert werden. Dies 
könnte man mit Tools, wie Elastic Search durchsucht werden und so Statistiken 
generiert werden. So können Fehler schneller und preventiv gefunden werden.

Ebenfalls würde man Interaktionen, wie Klicks und Mausbewegungen, an den Server senden, um
zu analysieren, wie die Users die Seite benutzen und wie man sie Optimieren könnte (weniger 
Mausklicks, bessere Orte der Knöpfe, ...).

Datenschutz (nicht umgesetzt)
===========
Unsere Webseite bräuchte eine AGB und Datenschutzerklärung, da wir Daten der Nutzer, wie 
Adresse, Email und ev. sogar Zahlungsdaten, speichern würden (dies ist allerdings nicht 
umgesetzt). 
Zusätzlich müsste man auch noch dem Nutzer erlauben, seine Daten einzusehen, herunter 
zu laden oder die Daten zu löschen.

Migrationsstrategie (halb umgesetzt)
===================
Unseres Projekt verwendet Flyway für die Datenbanken Migration. Dabei
erstellt man Scripts (oder auch Java Code), welche die DB von einer Version zur
nächsten nehmen. Der momentane Stand wird von Flyway in einer Tabelle verwaltet und
automatisch angepasst.

Wir sind uns unsicher, welche Migrierungsstrategie dies nun ist, da wenn man
die Applikation migriert, Flyway gleichzeitig die DB mit migriert.

Unserer "Migrationsprozess" würde folgendermassen aussiehen:
1) Docker Container erstellen
2) Container auf dem Server updaten und starten
3) Flyway updatet die DB automatisch
4) Fertig! Die Applikation ist migriert


Sicherheit
==========
Damit die Applikation sicher ist, werden natürlich SSL Zertifikate benützt, wie es heute üblich ist.
Dies schützt die Applikation allerdings nicht von Bugs, welche die Sicherheit beeinflusst oder üblie Dinge,
wie Cross Site Scripting ermöglichen. 
Die Applikation soll in einem Docker Container (nicht als Root im Container!) laufen, was auch ein sehr 
kleiner Schutz bieten kann.

Sicherheitslücken
-----------------
Um Sicherheitslücken zuvermeiden, würde man die Seite Pem Testen lassen umd mögliche Bugs bereits zu finden 
und zubeheben. Ebenfalls würden wir die Logfiles analysieren (mit einem Tool wie ElasticSearch) um möglicherweisse
Attacken, wie DDOS oder Bruteforce Angriffe, frühzeitig zu erkennenn und entsprechende Massnahmen dagegen vornehmen.

Die Server würden mit einer verschlüsselten Partion laufen, um zu vermeidenn, dass der Server Provider zugriff auf die
Daten hätte zu dem werden SSH-Keys in der Infrasturktur verwendet oder sehr sichere Passwörter, wo es sich nicht vermeiden
lässt.

Risiken, wie Bruteforce Angriffe, können auch über ein Nginx-Proxy, entschärft werden, da es schwieriger wird, diese 
durch zuführen.

HTTPS
----

Für das HTTPS Zertifikat gibt es zwei grobe Routen: Let's Encrypt und ein Zertifikat bei einem Provider kaufen.

### Let's Encrypt
Der Vorteil von Let's Encrypt ist, dass es zum einen nichts kostet und man (in der Regel) es einmal aufsetzt 
und danach nichts mehr ändern  muss. Zertifikate sind 90 Tage gültig und erneuern sich automatisch und 
die Installation ist kinderleicht. Einfach den entsprechenden Reverse Proxy Eintrag in Nginx oder Apache 
erstellen und `sudo certbot` laufen lassen. Certbot holt sich die Zertifkate automatisch und generiert die 
SSL konfiguration für den Reverse Proxy. Alternative könnte man auch nur das Zertifikat herunterladen.

Der Nachteil von Let's Encrypt ist, dass jeder ein Let's Encrypt Zertifikat automatisch beantragen kann.
Es sagt nichts über die Legitimität und Seriösität des Geschäftes aus.

### Zertifikat kaufen
Ein Zertifikat von z.B. GlobalSign (Google.com hat GlobalSign als Root Zertifikat) kann etwas über die 
Seite aussagen, wie dass sie seriös ist. Die Zertifikate, die dies tun nennen sich Organization Validated (OV) Certificate
und Extended Validation (EV) Certificate, während die Standart Zertifikate Domain Validated (DV) Certificate heissten.
Für ein OV Zertifikate wird geprüft, ob ein Geschäft wirklich existieren und alles korekt ist. EV Zertifikate sind
noch eine Stuffe höher und es wird noch mehr geprüft. 
DV Zertifikate sagen bloss aus, dass die Domain wirklich einem selbst gehört, allerdings nicht ob das Geschäft dahinter 
legitim ist. DV Zertifikate werden von Diensten wie Let's Encrypt ausgestellt.

Zudem hat man den Vorteil, dass man das Verteilen der Zertifiakte 
selbst verwalten und kontrollieren kann und nicht von Let's Encrypt ein Vorgehen diktiert wird.
Der Nachteil, dies *kann* teuer werden.

Als noch ein weiterer Bonus, kann man mit den EV Zertifikaten sein Firmenlogo neben dem Padlock im Browser 
anzeigen lassen.

### Fazit
Wäre dies ein seriöses Webshop Geschäft, wie Digitec, würden wir sicher in ein EV oder mindestens ein OV Zertifikat 
investieren um zu beweissen, das diese Webseite wirklich uns gehört und dies ein echtes Geschäft ist.

Da dies "nur" eine Aufgabe ist, wird allerdings ein Let's Encrypt Zertifikat reichen müssen.

Notfall Konzept
===============
Es gibt einige Szenarios, in welchen wir ein sehr grosses Problem hätte, wie 
- man kann nicht mehr Zahlen/Produkte kaufen
- der Server ist down
- Der Container ist down
- DNS oder Domain ist weg
- Mailserver nicht verfügbar

Da die Seite hinter einem Load-Balancer müsste laufen, wäre es kein Problem, wenn ein Server down geht, 
so lange es nicht der Loadbalancer selbst ist (In diesemfall würde man den Backup Loadbalancer hochfahren und manuel 
darauf switchen oder ev. lässt sich dies sogar automatisieren).
Da die Seite in einem Container läuft, kann man einfach wieder eine neue Instanz hochfahren.
Der SQL Server wäre problematischer, bei diesem würde man viele Backups erstellen und im Fall, dass 
der Server hops geht, dies einspielen (nach dem man ein Backup des "kaputten" Server erstellt hat - für alle Fälle). 

Bei externen Services, wie Zahlungsproviders ist es nicht so einfach ein Backup 
bereit zustellen. In diesem Fall müsste man den Vertragspartner anfragen.

Den Mailserver kann man selbst hosten und so wieder mit der vorherigen Strategie von Containers arbeiten und ein Failover
Server bereit halten.

### Backup
Ein inkrementelles Backup des SQL Servers sollte mindestens zweimal pro Tag durchgeführt werden, da man den Datenverlust umbedingt vermeiden soll.
Ein grosses Backup könnte man in einem grösseren Interval, wie 10 Tage erstellen.

Die Server selbst würde man mit einem Program, wie Puppets oder Ansible erstellen, so dass man nicht manuell ein Server anfassen muss und
automatisch neue Instanzen erstellen könnte.

Risiken
=======
Es gibt viele Risiken bei dennen man mehr oder weniger schauen kann das sie nicht eintreten.

- Der Server ist down
- Stromausfall bei der Infrastruktur
- Domain ist abgelafen
- Container ist down

Ein Risiko das man verringern kann ist das die Domain abläuft.
Dieses Risiko kann man umgehen in dem man die Domain immer vorzeitig verlängert und sie gar nicht erst ablaufen lässt.

Ein Risiko das immer besteht ist das der Sever down geht und somit die Seite nicht mehr erreichbar ist.
Um dieses Problem vorzeitig zu erkennen muss man den Server überwachen zum Beispiel muss man den Diskspace und die Ausslastung des Servers analysieren, eben so sollte man Log Files schreiben.
Wenn das Problem dennoch auftritt sollte man einen zweiten Server haben der vollständig aufgesetzt ist und möglicherweise auf einem anderen Netz läuft, dann könnt man einfach auf diesen Umleiten während man den andere fixt. 


Testkonzept
===========

(beide)
Login
-----

Warenkorb
---------


Links und externe Dokumente
===========================

-   Trello:
    > [[https://trello.com/b/Yz3XcD9R/m150]{.ul}](https://trello.com/b/Yz3XcD9R/m150)

-   GitLab:
    > [[https://gitlab.com/PanikSchale2001/m150_webshop]{.ul}](https://gitlab.com/PanikSchale2001/m150_webshop)


REST Endpoints
===========================
Diese Liste ist nicht vollständig...
Aber da wir diese bereits hatten, wollten wir die Liste nicht löschen.

### Produkte-Get
```
Type: Get-Request
Return: {pagesLeft, [products]}
Url: /api/prducts/{page}
Fehlerfälle: Wenn page ungültig dann "Bad Request 400" Beispiel User
```
### Produkt-Get
```
Type: Get-Request
Return: {product}
Url: /api/prducts/{productId}
Fehlerfälle: Wenn product ungültig = "Bad Request 400"
```

### Cart - Get
```
Type: Get-Request
Return: {Cart}
URL: /api/cart?cart=id
Fehlerfälle: if id == null| !id.notExist dann neuer Warenkorb
```

### Cart - Post
```
Type: Post-Request
Return: {Cart}
URL: /api/cart/{cart}/add/{product}/{addCounter}
Fehlerfälle: param invalid = "Bad Request 400"
```

### Image - Get
```
Type: Get-Request
Return: Image.jpag
URL: /api/product/{productId}/images/{count}
Fehlerfälle: param invalid = "Bad Request 400"
```
